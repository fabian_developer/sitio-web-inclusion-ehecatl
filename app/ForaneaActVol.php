<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForaneaActVol extends Model
{
    //tabla foranea
    protected $table='tblactividad_voluntariado'; 
    public $timestamps = false;
    protected $primaryKey= 'intid_activ_volun';
    protected $fillable = ['intid_activ_volun', 'intidvoluntariado','intidactividad'];
}
