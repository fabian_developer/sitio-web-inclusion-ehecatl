<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VoluntarioMailBienvenida extends Mailable
{
    use Queueable, SerializesModels;

    public $nombre;
    public $correo;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $password, $name)
    {
        $this->correo   = $email;
        $this->password = $password;
        $this->nombre   = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.email_bienvenida')
            ->from('tecnologias1997@gmail.com')
            ->subject('Programa de voluntariado');

    }
}
