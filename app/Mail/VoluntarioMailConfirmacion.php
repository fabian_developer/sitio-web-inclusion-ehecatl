<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VoluntarioMailConfirmacion extends Mailable
{
    use Queueable, SerializesModels;
    
    public $codi;
    public $nom;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($codigo,$nombre)
    {
        $this->codi = $codigo;
        $this->nom=$nombre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('email.email_confirmacion')
                ->from('tecnologias1997@gmail.com')
                ->subject('Programa de voluntariado');
    }
}
