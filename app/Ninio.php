<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ninio extends Model
{
    protected $table      = 'tblninio';
    public $timestamps    = false;
    protected $primaryKey = 'tblninio';
    protected $fillable   = ['inIdNinio', 'vchNombre', 'vchApellidoPa', 'vchApellidoMa', 'vchSexo', 'dteFechaNac', 'vchRefDomicilio', 'intEdad', 'dbePeso', 'dbeEstatura', 'vchTipoSangre'];
}
