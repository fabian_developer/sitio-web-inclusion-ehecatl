<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    protected $table='tbllugar'; 
    public $timestamps = false;
    protected $primaryKey= 'intidlugar';
    protected $fillable = ['intidlugar', 'vchlugar'];
}
