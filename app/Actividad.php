<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table='tblactividad'; 
    public $timestamps = false;
    protected $primaryKey= 'intidactividad';
    protected $fillable = ['intidactividad', 'vchnombre', 'vchdescripcion','vchfecha','vchhora_inicio','vchhora_termino','intnum_voluntarios','intnum_ninios','intidlugar','intid_tipo_transporte','vchestado'];   
}
