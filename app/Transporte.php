<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transporte extends Model
{
    protected $table='tbltipo_transporte'; 
    public $timestamps = false;
    protected $primaryKey= 'intid_tipo_transporte';
    protected $fillable = ['intid_tipo_transporte', 'vchtipo_transporte'];
}
