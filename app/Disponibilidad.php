<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disponibilidad extends Model
{
    protected $table='tbldisponibilidad'; 
    public $timestamps = false;
    protected $primaryKey= 'intiddisponibilidad';
    protected $fillable = ['intiddisponibilidad', 'vchdisponibilidad'];
}
