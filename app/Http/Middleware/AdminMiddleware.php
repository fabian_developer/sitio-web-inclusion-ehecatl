<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //pregunta el usuario esta logueado
        if (!auth()->check())
        {
            //redirecciona al pagina de login
            return redirect('/login');
        }
        //si el usuario logueado es diferente a administrador
        if (auth()->user()->tipo_usuario!=0) //si es diferente a 0       
        {
            //redirecciona a la pagina home
            return redirect('home');
        }
        return $next($request);
    }
}
