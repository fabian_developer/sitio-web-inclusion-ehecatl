<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //pregunta el usuario esta logueado
        if (!auth()->check())
        {
            //redirecciona al pagina de login
            return redirect('/login');
        }
        //si el usuario logueado es diferente a usuario normal
        if (auth()->user()->tipo_usuario!=1) //si es diferente a 1       
        {
            //redirecciona a la pagina home
            return redirect('home');
        }
        return $next($request);
    }
}
