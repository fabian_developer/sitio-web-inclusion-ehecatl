<?php

namespace App\Http\Controllers;

use App\Noticias;

class ConocenosController extends Controller
{
    //
    public function index()
    {
        $noticias = Noticias::all()->take(3);
        return view('conocenos')->with(['noticias' => $noticias]);
    }
}
