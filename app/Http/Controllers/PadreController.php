<?php
namespace App\Http\Controllers;

use App\Voluntario;
use Datatables;
use DB;

class PadreController extends Controller
{
    public function index_padre()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;
        return view('usuario-admin.padres')->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);
    }

    public function tabla_padres()
    {
        return datatables()->of(DB::select('call proc_vista_padres()'))->toJson();
    }
}
