<?php

namespace App\Http\Controllers;

use App\Disponibilidad;
use App\Mail\VoluntarioMailBienvenida;
use App\Mail\VoluntarioMailConfirmacion;
use App\Mail\VoluntarioMailRechazado;
use App\Ocupacion;
use App\User;
use App\Voluntario;
use Datatables;
use Illuminate\Http\Request;
use Mail;
use Validator;

class VoluntarioController extends Controller
{
    //=======================================================================================//
    //                                   Vista final                                         //
    //=======================================================================================//

    //funcion para la vista
    public function index()
    {
        //$datos = DB::table('tblvoluntariado')->where('vchtestimonio', '!=',"");
        $testimonio     = Voluntario::all()->where('vchestado', "Aceptado")->take(5);
        $ocupacion      = Ocupacion::all();
        $disponibilidad = Disponibilidad::all();

        return view('voluntariado')->with(['testimonio' => $testimonio])->with(['ocupacion' => $ocupacion])->with(['disponibilidad' => $disponibilidad]); //vista
    }

    //funcion para guardar
    public function guardardatos(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'nombre'              => 'required|min:3|max:250',
                'apellidos'           => 'required|min:3|max:250',
                'direccion'           => 'required|min:15|max:500',
                'telefono'            => 'required|min:8|max:12',
                'email'               => 'required|min:15|max:250|email',
                'genero'              => 'required|min:5|max:50|string',
                'ocupacion'           => 'required|min:5|max:250',
                'fecha_de_nacimiento' => 'required|min:5|max:50',
                'disponibilidad'      => 'required|min:5|max:50',
                'justificacion'       => 'required|min:25|max:500',
                'fecha_registro'      => 'required|min:5|max:100',
                // 'estado' => 'required|min:5|max:50'
            ]);

        $error_array    = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $field_name => $messages) {
                $error_array[] = $messages;
            }
        } else {
            $file     = $request->file('foto_perfil');
            $fileName = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/voluntario/foto/', $fileName);

            $voluntario                      = new Voluntario();
            $voluntario->vchnombre           = $request->nombre; //vchnombre;
            $voluntario->vchapellidos        = $request->apellidos;
            $voluntario->vchdireccion        = $request->direccion;
            $voluntario->vchtelefono         = $request->telefono;
            $voluntario->vchemail            = $request->email;
            $voluntario->vchgenero           = $request->genero;
            $voluntario->vchocupacion        = $request->ocupacion;
            $voluntario->vchfecha_nacimiento = $request->fecha_de_nacimiento;
            $voluntario->vchdisponibilidad   = $request->disponibilidad;
            $voluntario->vchporque           = $request->justificacion;
            $voluntario->vchfoto_perfil      = $fileName;
            $voluntario->vchfecha_registro   = $request->fecha_registro;
            $voluntario->vchtestimonio       = "p";
            $voluntario->vchestado           = "En espera"; //$request->vchestado;
            $voluntario->save();
            //return Response()->json($voluntario);
            $success_output = 'success';
        }
        $output = array(
            'error'   => $error_array,
            'success' => $success_output,
        );
        echo json_encode($output);
    }

    //=======================================================================================//
    //                                                                                       //
    //=======================================================================================//

    //=======================================================================================//
    //                                   Administrador                                       //
    //=======================================================================================//

    //funcion para mostrar la vista voluntario
    public function index_voluntario()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;
        return view('usuario-admin.voluntarios')->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);

        //return view('usuario-admin.voluntarios'); //vista productos desde la carpeta usuario-admin/
    }

    // public function cuenta_espera()
    // {
    // $contados_en_espera = DB::table('tblvoluntariado')
    //              ->select(DB::raw('COUNT(vchestado) as vchestado'))
    //              ->where('vchestado', '=', 'En espera')
    //              ->get();
    // }

    //funcion para mostrar los voluntarios
    public function getdatavoluntario()
    {
        //return datatables()->of(Voluntario::all()->where('vchestado','=',"En espera"))->toJson();

        // $consulta=DB::table('tblvoluntariado')->orderBy('intidvoluntariado','desc')->get();
        // return datatables()->of($consulta)->toJson();

        //return datatables()->of(Voluntario::all())->toJson();
        return datatables()->of(Voluntario::all()->where('vchestado', '!=', "Administrador"))->toJson();
    }

    //funcion para cambiar el estatus del voluntario
    public function modificaestatus(Request $request)
    {
        $msj_exito     = '';
        $ya_calificado = '';

        $voluntariado = Voluntario::find($request->clave);
        $estatus      = $voluntariado->vchestado;

        if ($estatus != "Aceptado" && $estatus != "Rechazado") {
            $respuesta               = $request->estatus;
            $voluntariado->vchestado = $respuesta; //inserta el dato estado
            $correo                  = $request->email;
            $nombre                  = $request->nombre;
            $voluntariado->save();

            if ($respuesta == "Aceptado") {
                $pass   = str_random(15);
                $codigo = str_random(70);
                //crear cuenta usuario
                $user                      = new User();
                $user->name                = $nombre;
                $user->email               = $correo;
                $user->password            = $pass;
                $user->tipo_usuario        = "1"; //1 : usuario voluntario
                $user->confirmado          = 0;
                $user->codigo_confirmacion = $codigo;
                $user->save();
                // al crear el nuevo usuario puede que muestre que no se pudo crear el nuevo
                //usuario ya que el correo electronico no se puede repetir

                //mandar correo electronico
                Mail::to($correo)->send(new VoluntarioMailConfirmacion($codigo, $nombre));
            } else if ($respuesta == "Rechazado") {
                Mail::to($correo)->send(new VoluntarioMailRechazado);
            }
            $msj_exito = 'success';
            //return Response()->json($voluntariado);
        } else {
            //mensaje de que el usuario ya fue calificado
            $ya_calificado = "calificado";
        }

        $arreglo = array(
            'error'   => $ya_calificado,
            'success' => $msj_exito,
        );
        echo json_encode($arreglo);
    }

    //función para verificar la cuenta creada
    public function verify($code)
    {
        $user                      = User::where('codigo_confirmacion', $code)->first();
        $user->confirmado          = true;
        $user->codigo_confirmacion = null;
        $name                      = $user->name;
        $email                     = $user->email; //recupera el correo
        $password                  = $user->password; //recupera la contraseña
        $pas                       = bcrypt($password); //encripta la contraseña recuperada
        $user->password            = $pas; //guarda la contraseña ya encriptada
        $user->save();

        //manda correo electronico en donde manda el usuario y la contraseña
        Mail::to($email)->send(new VoluntarioMailBienvenida($email, $password, $name));
        return redirect('/login');
    }

    //=======================================================================================//
    //                                                                                       //
    //=======================================================================================//

    //=======================================================================================//
    //                                   Voluntario                                          //
    //=======================================================================================//

    //vista principal usuario voluntario
    public function principal_voluntario()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;
        return view('usuario-voluntario.index')->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);
    }

    //=======================================================================================//
    //                                                                                       //
    //=======================================================================================//

}
