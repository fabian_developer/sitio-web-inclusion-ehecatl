<?php

namespace App\Http\Controllers;

use App\Actividad;
use App\Lugar;
use App\Transporte;
use App\Voluntario;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Validator;

class ActividadController extends Controller
{
    //=======================================================================================//
    //                                   Vista voluntario                                    //
    //=======================================================================================//

    //vista actividad
    public function index_voluntario()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;

        $lugar      = Lugar::all();
        $transporte = Transporte::all();
        return view('usuario-voluntario.actividades')->with(['lugar' => $lugar])->with(['transporte' => $transporte])->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);
    }

    //funcion para mostrar las actividades
    public function getdataactividades()
    {
        return datatables()->of(Actividad::all())->toJson();
    }

    //funcion para guardar
    public function guardardatos(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'actividad'    => 'required|min:10|max:250',
                'descripcion'  => 'required|min:10|max:500',
                'fecha'        => 'required|min:6|max:50',
                'hora_inicio'  => 'required|min:2|max:50',
                'hora_termino' => 'required|min:2|max:50',
                'voluntarios'  => 'required|min:1|max:5',
                'ninios'       => 'required|min:1|max:10',
                'lugar'        => 'required|min:1|max:5',
                'transporte'   => 'required|min:1|max:5',
            ]);

        $error_array  = array();
        $notificacion = '';

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $field_name => $messages) {
                $error_array[] = $messages;
            }
        } else {
            $clave = $request->clave;

            //actualizacion
            if ($clave != "") {
                $actividad = Actividad::find($clave);
                $estatus   = $actividad->vchestado;
                if ($estatus == "En espera") {
                    $actividad->vchnombre             = $request->actividad;
                    $actividad->vchdescripcion        = $request->descripcion;
                    $actividad->vchfecha              = $request->fecha; //
                    $actividad->vchhora_inicio        = $request->hora_inicio;
                    $actividad->vchhora_termino       = $request->hora_termino;
                    $actividad->intnum_voluntarios    = $request->voluntarios;
                    $actividad->intnum_ninios         = $request->ninios;
                    $actividad->intidlugar            = $request->lugar;
                    $actividad->intid_tipo_transporte = $request->transporte;
                    $actividad->save();
                    $notificacion = 'update';
                } else {
                    $notificacion = 'calificado';
                }
            } else {
                // $fecha_nueva= "2018-09-26";//$request->fecha;
                // $acti = DB::table('tblactividad')->where('vchfecha',$fecha_nueva)->first();
                // $fecha= $acti->vchfecha;//base

                // $datos=Actividad::all()->where('vchfecha','=',$fecha_nueva)->first();
                // $fech= $datos->vchfecha;//base

                // if ($fecha==$fecha_nueva)
                // {
                //    $notificacion = 'fecha_repetida';
                // }
                // else
                // {
                //guardado
                $actividad = new Actividad();
                //$fecha_nueva= "2018-09-26";//$request->fecha;
                $actividad->vchnombre             = $request->actividad;
                $actividad->vchdescripcion        = $request->descripcion;
                $actividad->vchfecha              = $request->fecha; //$fecha_nueva;
                $actividad->vchhora_inicio        = $request->hora_inicio;
                $actividad->vchhora_termino       = $request->hora_termino;
                $actividad->intnum_voluntarios    = $request->voluntarios;
                $actividad->intnum_ninios         = $request->ninios;
                $actividad->intidlugar            = $request->lugar;
                $actividad->intid_tipo_transporte = $request->transporte;
                $actividad->vchestado             = 'En espera';
                $actividad->save();
                $notificacion = 'save';
                //falta validar que no haya varias actividades en el mismo día

                // }
            }
        }
        $arreglo = array(
            'error'   => $error_array,
            'success' => $notificacion,
        );
        echo json_encode($arreglo);
    }

    //funcion para borrar actividad
    public function deletedata(Request $request)
    {
        $notificacion = '';
        $actividad    = Actividad::find($request->clave);
        $estatus      = $actividad->vchestado;
        if ($estatus == "En espera") {
            $actividad->delete();
            $notificacion = 'success';
        } else {
            $notificacion = 'calificado';
        }
        //return Response()->json($actividad);
        $arreglo = array(
            'success' => $notificacion,
        );
        echo json_encode($arreglo);
    }
//=======================================================================================//
    //                                                                                       //
    //=======================================================================================//

//=======================================================================================//
    //                                Vista administrador                                    //
    //=======================================================================================//

    //vista actividad
    public function index_administrador()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;

        return view('usuario-admin.actividades')->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);
    }

    //funcion para mostrar las actividades
    public function tabla_actividades()
    {
        //return datatables()->of(Actividad::all())->toJson();
        return datatables()->of(DB::select('call proc_vista_actividades()'))->toJson();
        //
    }

    //=======================================================================================//
    //                                                                                       //
    //=======================================================================================//

}
