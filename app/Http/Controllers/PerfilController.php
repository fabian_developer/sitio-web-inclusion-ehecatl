<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use Response;
use App\Voluntario;
use App\Ocupacion;
use App\Disponibilidad;

class PerfilController extends Controller
{

    //=======================================================================================//
    //                                   Vista final                                         //
    //=======================================================================================//

    //vista perfil
    public function index()
    {
        $ocupacion_=Ocupacion::all();
        $disponibilidad_=Disponibilidad::all(); 

        $email=auth()->user()->email;
        $datos=Voluntario::all()->where('vchemail', $email)->first();
        $direccion= $datos->vchdireccion;//base
        $telefono=$datos->vchtelefono;
        $ocupacion=$datos->vchocupacion;
        $fecha_nacimiento=$datos->vchfecha_nacimiento;
        $genero=$datos->vchgenero;
        $nombre=$datos->vchnombre;
        $apellidos=$datos->vchapellidos;
        $foto=$datos->vchfoto_perfil;
        $disponibilidad=$datos->vchdisponibilidad;
        $testimonio=$datos->vchtestimonio;

    	return view('usuario-voluntario.perfil')->with(['ocupacion_' => $ocupacion_])->with(['disponibilidad_' => $disponibilidad_])->with('disponibilidad',$disponibilidad)->with('foto',$foto)->with('nombre',$nombre)->with('apellidos',$apellidos)->with('email',$email)->with('direccion',$direccion)->with('telefono',$telefono)->with('ocupacion',$ocupacion)->with('fecha_nacimiento',$fecha_nacimiento)->with('genero',$genero)->with('testimonio',$testimonio); //vista
    }

    public function modificaperfil(Request $request)
    {
        $voluntariado=Voluntario::find($request->clave);
        $respuesta=$request->estatus;
        $voluntariado->vchestado= $respuesta; //inserta el dato estado
        $correo= $request->email;
        $nombre=$request->nombre;
        $voluntariado->save();
    }

    
    //=======================================================================================//
    //                                                                                       //
    //=======================================================================================//

    

    //=======================================================================================//
    //                                   Administrador                                       //
    //=======================================================================================//








    

}
