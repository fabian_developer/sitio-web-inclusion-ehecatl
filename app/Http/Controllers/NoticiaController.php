<?php

namespace App\Http\Controllers;

use App\Noticias;
use DB;

class NoticiaController extends Controller
{
    //función para verificar la cuenta creada
    public function noticia($titulo)
    {
        $noticia = Noticias::where('vchtitulo', $titulo)->first();
        // return $titulo;
        return view('noticia')->with(['noticia' => $noticia]);
    }

    //actualidad
    public function index_actualidad()
    {
        //$noticias = Noticias::paginate();
        $noticias = DB::table('tblnoticias')->paginate(3);
        return view('actualidad')->with(['noticias' => $noticias]);
    }
}
