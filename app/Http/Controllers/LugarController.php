<?php
namespace App\Http\Controllers;

use App\Lugar;
use App\Voluntario;
use Datatables;
use Illuminate\Http\Request;
use Response;
use Validator;

class LugarController extends Controller
{
    //mostrar la vista lugar
    public function index()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;

        //return view('usuario-admin.lugares'); //vista lugar
        return view('usuario-admin.lugares')->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);
    }

    //funcion para mostrar los lugares
    public function getdatalugares()
    {
        return datatables()->of(Lugar::all())->toJson();
    }

    //funcion para guardar
    public function guardardatos(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'lugar' => 'required|min:10|max:250',
            ]);

        $error_array  = array();
        $notificacion = '';

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $field_name => $messages) {
                $error_array[] = $messages;
            }
        } else {
            $clave = $request->clave;

            if ($clave != "") {
                $lugar           = Lugar::find($clave);
                $lugar->vchlugar = $request->lugar;
                $lugar->save();
                $notificacion = 'update';
            } else {
                $lugar           = new Lugar();
                $lugar->vchlugar = $request->lugar;
                $lugar->save();
                $notificacion = 'save';
            }
        }

        $arreglo = array(
            'error'   => $error_array,
            'success' => $notificacion,
        );
        echo json_encode($arreglo);
    }

    //funcion para borrar lugares
    public function deletedata(Request $request)
    {
        $lugar = Lugar::find($request->clave);
        $lugar->delete();
        return Response()->json($lugar);
    }
}
