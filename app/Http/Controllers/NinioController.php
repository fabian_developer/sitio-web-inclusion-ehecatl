<?php

namespace App\Http\Controllers;

use App\Voluntario;
use Datatables;
use DB;

class NinioController extends Controller
{

    public function index_ninio()
    {
        $email     = auth()->user()->email;
        $datos     = Voluntario::all()->where('vchemail', $email)->first();
        $nombre    = $datos->vchnombre;
        $apellidos = $datos->vchapellidos;
        $foto      = $datos->vchfoto_perfil;
        return view('usuario-admin.ninios')->with('email', $email)->with('nombre', $nombre)->with('apellidos', $apellidos)->with('foto', $foto);
    }

    public function tabla_ninios()
    {
        return datatables()->of(DB::select('call proc_vista_ninios()'))->toJson();
    }
}
