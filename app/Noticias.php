<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    protected $table      = 'tblnoticias';
    public $timestamps    = false;
    protected $primaryKey = 'intidnoticia';
    protected $fillable   = ['intidnoticia', 'vchtitulo', 'vchparrafo1', 'vchparrafo2', 'vchparrafo3', 'vchparrafo4', 'vchparrafo5', 'vchparrafo6', 'vchparrafo7', 'vchparrafo8', 'vchparrafo9', 'vchparrafo10', 'vchfecha', 'vchimagen'];
}
