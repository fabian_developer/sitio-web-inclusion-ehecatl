<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
    protected $table='tblocupacion'; 
    public $timestamps = false;
    protected $primaryKey= 'intidocupacion';
    protected $fillable = ['intidocupacion', 'vchocupacion'];
}
