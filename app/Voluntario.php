<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voluntario extends Model
{
    protected $table      = 'tblvoluntariado';
    public $timestamps    = false;
    protected $primaryKey = 'intidvoluntariado';
    protected $fillable   = ['intidvoluntariado', 'vchnombre', 'vchapellidos', 'vchdireccion', 'vchtelefono', 'vchemail', 'vchgenero', 'vchocupacion', 'vchfecha_nacimiento', 'vchdisponibilidad', 'vchporque', 'vchfoto_perfil', 'vchfecha_registro', 'vchtestimonio', 'vchestado'];
}
