<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
            <title>
                Programa Integral de Inclusión EHECATL
            </title>
            <!-- Favicon -->
            <link href="../assets2/images/logo/favicon.png" rel="shortcut icon">
                <!-- core dependcies css -->
                <link href="../assets2/vendor/bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>
                <link href="../assets2/vendor/PACE/themes/blue/pace-theme-minimal.css" rel="stylesheet"/>
                <link href="../assets2/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
                <!-- page css -->
                <link href="../assets2/vendor/jvectormap-master/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
                <!-- core css -->
                <link href="../assets2/css/font-awesome.min.css" rel="stylesheet">
                    <link href="../assets2/css/themify-icons.css" rel="stylesheet">
                        <link href="../assets2/css/materialdesignicons.min.css" rel="stylesheet">
                            <link href="../assets2/css/animate.min.css" rel="stylesheet">
                                <link href="../assets2/css/app.css" rel="stylesheet">
                                </link>
                            </link>
                        </link>
                    </link>
                </link>
            </link>
        </meta>
    </head>
    <body>
        <div id="app">
            <div class="app header-primary side-nav-dark">
                {{-- temas: info - warning header-info-gradient --}}
                <div class="layout">
                    <!-- Header START -->
                    <div class="header navbar">
                        <div class="header-container">
                            <div class="nav-logo">
                                <a href="index.html">
                                    <div class="logo logo-dark" style="background-image: url('../assets2/images/logo/logo.png')">
                                    </div>
                                    <div class="logo logo-white" style="background-image: url('../assets2/images/logo/logo-white.png')">
                                    </div>
                                </a>
                            </div>
                            <ul class="nav-left">
                                <li>
                                    <a class="sidenav-fold-toggler" href="javascript:void(0);">
                                        <i class="mdi mdi-menu">
                                        </i>
                                    </a>
                                    <a class="sidenav-expand-toggler" href="javascript:void(0);">
                                        <i class="mdi mdi-menu">
                                        </i>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav-right">
                                <li class="notifications dropdown dropdown-animated scale-left">
                                    <span class="counter">
                                        2
                                    </span>
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <i class="mdi mdi-email-outline">
                                        </i>
                                    </a>
                                </li>
                                <li class="user-profile dropdown dropdown-animated scale-left">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        <div class="media-img">
                                            <img alt="" class="img-fluid img-circle" src="../../voluntario/foto/{{ $foto }}" style="width: 35px; height: 35px">
                                                {{-- profile-img --}}
                                                <strong>
                                                    {{--  {{ $nombre }}  {{ $apellidos }} --}}
                                                </strong>
                                            </img>
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu dropdown-md p-v-0">
                                        <li>
                                            <ul class="list-media">
                                                <li class="list-item p-15">
                                                    <div class="media-img">
                                                        <img alt="Foto" src="../../voluntario/foto/{{ $foto }}">
                                                        </img>
                                                    </div>
                                                    <div class="info">
                                                        <span class="title text-semibold">
                                                            {{ $nombre }} {{ $apellidos }}
                                                        </span>
                                                        <span class="sub-title">
                                                            {{ $email }}
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="divider" role="separator">
                                        </li>
                                        {{--
                                        <li>
                                            <a href="#">
                                                <i class="ti-settings p-r-10">
                                                </i>
                                                <span>
                                                    Configuración
                                                </span>
                                            </a>
                                        </li>
                                        --}}
                                        <li>
                                            <a href="perfil">
                                                <i class="ti-user p-r-10">
                                                </i>
                                                <span>
                                                    Perfil
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <i class="ti-power-off p-r-10">
                                                </i>
                                                <span>
                                                    Cerrar sesión
                                                </span>
                                            </a>
                                            <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Header END -->
                    <!-- Side Nav START menu izquierda-->
                    <div class="side-nav expand-lg">
                        <div class="side-nav-inner">
                            <ul class="side-nav-menu scrollable">
                                <li class="side-nav-header">
                                    <span>
                                        Modulos
                                    </span>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="dropdown-toggle" href="index">
                                        <span class="icon-holder">
                                            <i class="mdi mdi-desktop-mac">
                                            </i>
                                        </span>
                                        <span class="title">
                                            Principal
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="dropdown-toggle" href="actividades">
                                        <span class="icon-holder">
                                            <i class="mdi mdi-presentation-play">
                                            </i>
                                        </span>
                                        <span class="title">
                                            Actividades
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="dropdown-toggle" href="#">
                                        <span class="icon-holder">
                                            <i class="mdi mdi-clipboard-text">
                                            </i>
                                        </span>
                                        <span class="title">
                                            Actualidad
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="dropdown-toggle" href="#">
                                        <span class="icon-holder">
                                            <i class="mdi mdi-email-outline">
                                            </i>
                                        </span>
                                        <span class="title">
                                            Mensajes
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Side Nav END -->
                    <!-- Page Container START -->
                    <div class="page-container">
                        <!-- Content Wrapper START -->
                        <div class="main-content">
                            <div class="container-fluid">
                                <div class="card">
                                    <div class="card-body">
                                        <br>
                                            <div class="row">
                                                <div class="col-md-4" style="text-aling:center">
                                                    <img alt="" class="img-fluid rounded mx-auto d-block img-thumbnail" src="../../voluntario/foto/{{ $foto }}">
                                                    </img>
                                                </div>
                                                {{-- img-fluid rounded-circle d-block mx-auto m-b-30 --}}
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <h2 class="m-b-5">
                                                                {{ $nombre }} {{ $apellidos }}
                                                            </h2>
                                                            <input id="nombre_" type="hidden" value="{{ $nombre }}">
                                                                <input id="apellidos_" type="hidden" value="{{ $apellidos }}">
                                                                    <br>
                                                                        <div class="form-group">
                                                                            <strong>
                                                                                Correo electronico
                                                                            </strong>
                                                                            <br>
                                                                                <h5>
                                                                                    {{ $email }}
                                                                                </h5>
                                                                                <input id="email_" type="hidden" value="{{ $email }}">
                                                                                </input>
                                                                            </br>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <strong>
                                                                                Dirección
                                                                            </strong>
                                                                            <br>
                                                                                <h5>
                                                                                    {{ $direccion }}
                                                                                </h5>
                                                                                <input id="direccion_" type="hidden" value="{{ $direccion }}">
                                                                                </input>
                                                                            </br>
                                                                        </div>
                                                                    </br>
                                                                </input>
                                                            </input>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <strong>
                                                                    Telefono
                                                                </strong>
                                                                <br>
                                                                    <h5>
                                                                        {{ $telefono }}
                                                                    </h5>
                                                                    <input id="telefono_" type="hidden" value="{{ $telefono }}">
                                                                    </input>
                                                                </br>
                                                            </div>
                                                            <div class="form-group">
                                                                <strong>
                                                                    Ocupación
                                                                </strong>
                                                                <br>
                                                                    <h5>
                                                                        {{ $ocupacion }}
                                                                    </h5>
                                                                    <input id="ocupacion_" type="hidden" value="{{ $ocupacion }}">
                                                                    </input>
                                                                </br>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <strong>
                                                                    Genero
                                                                </strong>
                                                                <br>
                                                                    <h5>
                                                                        {{ $genero }}
                                                                    </h5>
                                                                    <input id="genero_" type="hidden" value="{{ $genero }}">
                                                                    </input>
                                                                </br>
                                                            </div>
                                                            <div class="form-group">
                                                                <strong>
                                                                    Fecha de nacimiento
                                                                </strong>
                                                                <br>
                                                                    <h5>
                                                                        {{ $fecha_nacimiento }}
                                                                    </h5>
                                                                    <input id="fecha_nacimiento_" type="hidden" value="{{ $fecha_nacimiento }}">
                                                                    </input>
                                                                </br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <strong>
                                                                    Disponibilidad
                                                                </strong>
                                                                <br>
                                                                    <h5>
                                                                        {{ $disponibilidad }}
                                                                    </h5>
                                                                    <input id="disponibilidad_" type="hidden" value="{{ $disponibilidad }}">
                                                                    </input>
                                                                </br>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input class="btn btn-block btn-default" id="btnModificar" name="btnModificar" type="button" value="Actualizar datos">
                                                            </input>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </br>
                                    </div>
                                </div>
                                <div class="card">
                                    {{--
                                    <div class="p-20">
                                        <h4 class="card-title m-b-0">
                                            Historial
                                        </h4>
                                    </div>
                                    --}}
                                    <div class="tab-danger">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#tab-warning-1" role="tab">
                                                    Actividades
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tab-testimonio" role="tab">
                                                    Testimonio
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tab-warning-3" role="tab">
                                                    Configuración
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tab-warning-1" role="tabpanel">
                                                <div class="p-h-15 p-v-20">
                                                    <p>
                                                        Historial de las actividades
                                                    </p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-overflow">
                                                            <table cellspacing="0" class="table table-striped nowrap table-hover table-bordered cell-border order-column" id="tabla_actividades" name="tabla_actividades" width="100%">
                                                                <thead class="thead-light">
                                                                    <tr>
                                                                        <th>
                                                                            Clave
                                                                        </th>
                                                                        <th>
                                                                            Nombre
                                                                        </th>
                                                                        <th>
                                                                            Descripción
                                                                        </th>
                                                                        <th>
                                                                            Fecha
                                                                        </th>
                                                                        <th>
                                                                            Hora inicio
                                                                        </th>
                                                                        <th>
                                                                            Hora termino
                                                                        </th>
                                                                        <th>
                                                                            Voluntarios
                                                                        </th>
                                                                        <th>
                                                                            Niños
                                                                        </th>
                                                                        <th>
                                                                            Lugar
                                                                        </th>
                                                                        <th>
                                                                            Transporte
                                                                        </th>
                                                                        <th>
                                                                            Estatus
                                                                        </th>
                                                                        <th>
                                                                            Opciones
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab-testimonio" role="tabpanel">
                                                <div class="p-h-15 p-v-20">
                                                    <p>
                                                        {{ $testimonio }}
                                                    </p>
                                                    <br>
                                                        <input class="btn btn-default" type="button" value="Modificar">
                                                        </input>
                                                    </br>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab-warning-3" role="tabpanel">
                                                <div class="p-h-15 p-v-20">
                                                    <p>
                                                        Cambiar contraseña
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <strong>
                                                                    Ingresa la nueva contraseña
                                                                </strong>
                                                                <input class="form-control" id="" name="" type="text">
                                                                    <br>
                                                                        <strong>
                                                                            Repite la contraseña
                                                                        </strong>
                                                                        <input class="form-control" id="" name="" type="text">
                                                                            <br>
                                                                                <input class="btn btn-info" type="button" value="Guardar">
                                                                                </input>
                                                                            </br>
                                                                        </input>
                                                                    </br>
                                                                </input>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Wrapper END -->
                        <!-- Content Wrapper END -->
                        <!-- Footer START -->
                        <footer class="content-footer">
                            <div class="footer">
                                <div class="copyright">
                                    <span>
                                        © Copyright 2018.
                                        <b class="text-dark">
                                            Programa Integral de Inclusión EHECATL.
                                        </b>
                                        Todos los derechos
                                    reservados.
                                    </span>
                                </div>
                            </div>
                        </footer>
                        <!-- Footer END -->
                    </div>
                    <!-- Page Container END -->
                </div>
            </div>
            {{-- @yield('footer') --}}
        </div>
        {{-- Modal guardar y actualizar registro --}}
        <div class="modal fade" id="frm-modal-perfil">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#223143;">
                        <h5 class="modal-title" id="titulo_modal" style="color:white">
                        </h5>
                        <button aria-label="Close" class="close" data-dismiss="modal" style="color:white" type="button">
                            <span aria-hidden="true">
                                ×
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="p-15 m-v-40">
                            <form autocomplete="off" method="post">
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Nombre
                                            </label>
                                            {{--
                                            <input class="form-control" id="clave" name="clave" type="hidden" value="">
                                                --}}
                                                <input class="form-control" id="nombre" name="nombre" type="text">
                                                    <div id="request_nombre">
                                                    </div>
                                                </input>
                                            </input>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Apellidos
                                            </label>
                                            <input class="form-control" id="apellidos" name="apellidos" placeholder="Ingrese sus apellidos" type="text">
                                                <div id="request_apellidos">
                                                </div>
                                            </input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">
                                                Dirección
                                            </label>
                                            <textarea class="form-control" id="direccion" name="direccion" placeholder="Ingrese su dirección" rows="1" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;">
                                            </textarea>
                                            <div id="request_direccion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Correo electrónico
                                            </label>
                                            <input class="form-control" id="correo_electronico" name="correo_electronico" placeholder="Ingrese su correo electrónico" type="text">
                                                <div id="request_correo_electronico">
                                                </div>
                                            </input>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Número de teléfono
                                            </label>
                                            <input class="form-control" id="telefono" name="telefono" placeholder="Ingrese su número de teléfono" type="text">
                                                <div id="request_telefono">
                                                </div>
                                            </input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Genero
                                            </label>
                                            <input class="form-control" id="genero" name="genero" placeholder="Ingrese el lugar" type="text">
                                                <div id="request_genero">
                                                </div>
                                            </input>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Ocupación
                                            </label>
                                            <select class="form-control" id="ocupacion" name="ocupacion">
                                                @foreach ($ocupacion_ as $ocupacion_)
                                                <option value="{{ $ocupacion_->vchocupacion }}">
                                                    {{
                                                $ocupacion_->vchocupacion }}
                                                </option>
                                                @endforeach
                                            </select>
                                            <div id="request_ocupacion">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Fecha de nacimiento
                                            </label>
                                            <?php 
                                            //fecha actual
                                            $hoy = date("Y-m-d");
                                            //parte la fecha actual
                                            $array = explode("-", $hoy);
                                            $dia_actual = $array[2];
                                            $mes_actual = $array[1];
                                            $ano_actual = $array[0];
                                            //fecha maximo = año actual menos 18 años 
                                            $maximo=$ano_actual-18 . "-" . $mes_actual . "-" . $dia_actual;
                                            //fecha minimo = fecha actual menos 58 años
                                            $minimo=$ano_actual-68 . "-" . $mes_actual . "-" . $dia_actual;
                                        ?>
                                            <input class="form-control" id="fecha_de_nacimiento" max="{{ $maximo }}" min="{{ $minimo }}" name="fecha_de_nacimiento" placeholder="Seleccione su fecha de nacimiento" type="date">
                                                <div id="request_fecha_de_nacimiento">
                                                </div>
                                            </input>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Disponibilidad
                                            </label>
                                            <select class="form-control" id="disponibilidad" name="disponibilidad">
                                                @foreach ($disponibilidad_ as $disponibilidad_)
                                                <option value="{{ $disponibilidad_->vchdisponibilidad }}">
                                                    {{
                                                $disponibilidad_->vchdisponibilidad }}
                                                </option>
                                                @endforeach
                                            </select>
                                            <div id="request_disponibilidad">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">
                                                Foto
                                            </label>
                                            <input class="" id="foto_perfil" name="foto_perfil" placeholder="Seleccione una foto reciente" required="" type="file">
                                            </input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="form-control" id="token" name="token" readonly="" type="text" value="{{ csrf_token() }}"/>
                                    </div>
                                </div>
                                <span id="form_output">
                                </span>
                                <div class="modal-footer">
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-4">
                                        <input class="btn btn-block btn-default" id="btnCerrar" name="btnCerrar" type="button" value="Cerrar">
                                        </input>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="btn btn-block btn-info" id="btnGuardar" name="btnGuardar" type="button" value="Guardar">
                                        </input>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Modal guardar y actualizar registro --}} {{-- jquery --}}
        <script src="../assets2/js/jquery.min.js">
        </script>
        <!-- perfil -->
        <script src="../assets2/js-vol/perfil.js">
        </script>
        <!-- fin - perfil -->
        <script src="../assets2/js/vendor.js">
        </script>
        <script src="../assets2/js/app.min.js">
        </script>
        <!-- page js -->
        <script src="../assets2/vendor/chart.js/dist/Chart.min.js">
        </script>
        <script src="../assets2/vendor/datatables/media/js/jquery.dataTables.js">
        </script>
        <script src="../assets2/vendor/datatables/media/js/dataTables.bootstrap4.min.js">
        </script>
        <script src="../assets2/js/tables/data-table.js">
        </script>
        <script src="../js/sweetalert.min.js">
        </script>
    </body>
</html>