<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <!-- Favicon -->
        <link href="../assets2/images/logo/favicon.png" rel="shortcut icon"/>
        <!-- core dependcies css -->
        <link href="../assets2/vendor/bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>
        <link href="../assets2/vendor/PACE/themes/blue/pace-theme-minimal.css" rel="stylesheet"/>
        <link href="../assets2/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <!-- page css -->
        <link href="../assets2/vendor/jvectormap-master/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
        <!-- core css -->
        <link href="../assets2/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="../assets2/css/themify-icons.css" rel="stylesheet"/>
        <link href="../assets2/css/materialdesignicons.min.css" rel="stylesheet"/>
        <link href="../assets2/css/animate.min.css" rel="stylesheet"/>
        <link href="../assets2/css/app.css" rel="stylesheet"/>
    </head>
</html>
<body>
    <div id="app">
        <div class="app header-primary side-nav-dark">
            {{-- temas: info - warning header-info-gradient --}}
            <div class="layout">
                <!-- Header START -->
                <div class="header navbar">
                    <div class="header-container">
                        <div class="nav-logo">
                            <a href="index.html">
                                <div class="logo logo-dark" style="background-image: url('../assets2/images/logo/logo.png')">
                                </div>
                                <div class="logo logo-white" style="background-image: url('../assets2/images/logo/logo-white.png')">
                                </div>
                            </a>
                        </div>
                        <ul class="nav-left">
                            <li>
                                <a class="sidenav-fold-toggler" href="javascript:void(0);">
                                    <i class="mdi mdi-menu">
                                    </i>
                                </a>
                                <a class="sidenav-expand-toggler" href="javascript:void(0);">
                                    <i class="mdi mdi-menu">
                                    </i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="notifications dropdown dropdown-animated scale-left">
                                <span class="counter">
                                    2
                                </span>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="mdi mdi-email-outline">
                                    </i>
                                </a>
                            </li>
                            <li class="user-profile dropdown dropdown-animated scale-left">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <div class="media-img">
                                        <img alt="" class="img-fluid img-circle" src="../../voluntario/foto/{{ $foto }}" style="width: 35px; height: 35px">
                                            {{-- profile-img --}}
                                            <strong>
                                                {{--  {{ $nombre }} {{ $apellidos }} --}}
                                            </strong>
                                        </img>
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-md p-v-0">
                                    <li>
                                        <ul class="list-media">
                                            <li class="list-item p-15">
                                                <div class="media-img">
                                                    <img alt="" src="../../voluntario/foto/{{ $foto }}">
                                                    </img>
                                                </div>
                                                <div class="info">
                                                    <span class="title text-semibold">
                                                        {{ $nombre }} {{ $apellidos }}
                                                    </span>
                                                    <span class="sub-title">
                                                        {{ $email }}
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="divider" role="separator">
                                    </li>
                                    <li>
                                        <a href="perfil">
                                            <i class="ti-user p-r-10">
                                            </i>
                                            <span>
                                                Perfil
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="ti-power-off p-r-10">
                                            </i>
                                            <span>
                                                Cerrar sesión
                                            </span>
                                        </a>
                                        <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Header END -->
                <!-- Side Nav START menu izquierda-->
                <div class="side-nav expand-lg">
                    <div class="side-nav-inner">
                        <ul class="side-nav-menu scrollable">
                            <li class="side-nav-header">
                                <span>
                                    Modulos
                                </span>
                            </li>
                            <li class="nav-item dropdown open active">
                                <a class="dropdown-toggle" href="index">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-desktop-mac">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Principal
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="actividades">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-presentation-play">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Actividades
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="#">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-clipboard-text">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Actualidad
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="#">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-email-outline">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Mensajes
                                    </span>
                                </a>
                            </li>
                            {{--
                            <li class="side-nav-header">
                                <span>
                                    Reporte
                                </span>
                            </li>
                            --}}
                        </ul>
                    </div>
                </div>
                <!-- Side Nav END -->
                <!-- Page Container START -->
                <div class="page-container">
                    <!-- Content Wrapper START -->
                    <div class="main-content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body ">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-calendar font-size-40 icon-gradient-success text-success">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Próximas actividades
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0">
                                                        5
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-cup font-size-40 icon-gradient-info text-info">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Actividades completadas
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0">
                                                        8
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-star font-size-40 icon-gradient-primary text-primary">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Próximos eventos
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0 text-success">
                                                        <span>
                                                            12
                                                        </span>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Wrapper END -->
                    <!-- Footer START -->
                    <footer class="content-footer">
                        <div class="footer">
                            <div class="copyright">
                                <span>
                                    © Copyright 2018.
                                    <b class="text-dark">
                                        Programa Integral de Inclusión EHECATL.
                                    </b>
                                    Todos los derechos
                                    reservados.
                                </span>
                            </div>
                        </div>
                    </footer>
                    <!-- Footer END -->
                </div>
                <!-- Page Container END -->
            </div>
        </div>
        {{-- @yield('footer') --}}
    </div>
    {{-- Modal guardar y actualizar registro --}}
    {{--
    <div class="modal fade" id="frm-modal-lugares">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#223143;">
                    <h5 class="modal-title" id="titulo_modal" style="color:white">
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" style="color:white" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-15 m-v-40">
                        <form autocomplete="off" method="post">
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">
                                            Lugar
                                        </label>
                                        <input class="form-control" id="clave" name="clave" type="hidden">
                                            <input class="form-control" id="lugar" name="lugar" placeholder="Ingrese el lugar" type="text">
                                                <div id="request_lugar">
                                                </div>
                                            </input>
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control" id="token" name="token" readonly="" type="text" value="{{ csrf_token() }}"/>
                                </div>
                            </div>
                            <span id="form_output">
                            </span>
                            <div class="modal-footer">
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-block btn-default" id="btnCerrar" name="btnCerrar" type="button" value="Cerrar">
                                    </input>
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-block btn-info" id="btnGuardar" name="btnGuardar" type="button" value="Guardar">
                                    </input>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    --}}


    {{-- jquery --}}
    <script src="../assets2/js/jquery.min.js">
    </script>
    <!-- lugar -->
    {{--
    <script src="../assets2/lugar.js">
    </script>
    --}}
    <!-- fin - voluntario -->
    <script src="../assets2/js/vendor.js">
    </script>
    <script src="../assets2/js/app.min.js">
    </script>
    <!-- page js -->
    <script src="../assets2/vendor/chart.js/dist/Chart.min.js">
    </script>
    <script src="../assets2/vendor/datatables/media/js/jquery.dataTables.js">
    </script>
    <script src="../assets2/vendor/datatables/media/js/dataTables.bootstrap4.min.js">
    </script>
    <script src="../assets2/js/tables/data-table.js">
    </script>
    <script src="../js/sweetalert.min.js">
    </script>
</body>