<!DOCTYPE html>
<html>
{{-- class="no-js" lang="zxx" --}} {{-- lang="{{ app()->getLocale() }}" --}}

<head>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="SITE KEYWORDS HERE" /> {{--
	<meta name="description" content="">
	<meta name='copyright' content=''> --}}
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Title -->
	<title>Programa Integral de Inclusión EHECATL</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="assets/images/icono.png">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<!-- Fancy Box CSS -->
	<link rel="stylesheet" href="assets/css/jquery.fancybox.min.css">
	<!-- Owl Carousel CSS -->
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="assets/css/animate.min.css">
	<!-- Slick Nav CSS -->
	<link rel="stylesheet" href="assets/css/slicknav.min.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<!-- Learedu Stylesheet -->
	<link rel="stylesheet" href="assets/css/normalize.css">
	<link rel="stylesheet" href="assets/style.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
	<!-- Learedu Color -->
	<link rel="stylesheet" href="assets/css/color/color1.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
</head>

<body>
	<div id="app">

		<!-- Footer -->
        <footer class="footer overlay section wow fadeIn">
            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="bottom-head">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Social -->
                                        <ul class="social">
                                            <li>
                                                <a target="_blank" href="https://twitter.com/?lang=es">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://es-la.facebook.com/" >
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://www.youtube.com/?hl=es-419&gl=MX">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- End Social -->
                                        <!-- Copyright -->
                                        <div class="copyright">
                                            <p>© Copyright 2018. Programa Integral de Inclusión
                                                <a href="#">EHECATL</a>. Todos los derechos reservados.</p>
                                        </div>
                                        <!--/ End Copyright -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Footer Bottom -->
        </footer>
        <!--/ End Footer -->



	</div>

	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery-migrate.min.js"></script>
	<!-- Popper JS-->
	<script src="assets/js/popper.min.js"></script>
	<!-- Bootstrap JS-->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- Colors JS-->
	<script src="assets/js/colors.js"></script>
	<!-- Jquery Steller JS -->
	<script src="assets/js/jquery.stellar.min.js"></script>
	<!-- Particle JS -->
	<script src="assets/js/particles.min.js"></script>
	<!-- Fancy Box JS-->
	<script src="assets/js/facnybox.min.js"></script>
	<!-- Magnific Popup JS-->
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<!-- Masonry JS-->
	<script src="assets/js/masonry.pkgd.min.js"></script>
	<!-- Circle Progress JS -->
	<script src="assets/js/circle-progress.min.js"></script>
	<!-- Owl Carousel JS-->
	<script src="assets/js/owl.carousel.min.js"></script>
	<!-- Waypoints JS-->
	<script src="assets/js/waypoints.min.js"></script>
	<!-- Slick Nav JS-->
	<script src="assets/js/slicknav.min.js"></script>
	<!-- Counter Up JS -->
	<script src="assets/js/jquery.counterup.min.js"></script>
	<!-- Easing JS-->
	<script src="assets/js/easing.min.js"></script>
	<!-- Wow Min JS-->
	<script src="assets/js/wow.min.js"></script>
	<!-- Scroll Up JS-->
	<script src="assets/js/jquery.scrollUp.min.js"></script>
	<!-- Google Maps JS -->
	{{--
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyC0RqLa90WDfoJedoE3Z_Gy7a7o8PCL2jw"></script> --}}
	<script src="assets/js/gmaps.min.js"></script>
	<!-- Main JS-->
	<script src="assets/js/main.js"></script>
	<!-- Voluntariado -->
	<script src="assets/voluntariado.js"></script>
	<!-- sweetalert -->
	<script src="js/sweetalert.min.js"></script>
</body>

</html>
