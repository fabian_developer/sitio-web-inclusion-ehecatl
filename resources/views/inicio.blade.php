<!DOCTYPE html>
<html>
    {{-- class="no-js" lang="zxx" --}} {{-- lang="{{ app()->getLocale() }}" --}}
    <head>
        <!-- Meta -->
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="SITE KEYWORDS HERE" name="keywords"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <!-- Title -->
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <!-- Favicon -->
        <link href="assets/images/icono.png" rel="icon" type="image/png"/>
        <!-- Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"/>
        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Font Awesome CSS -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
        <!-- Fancy Box CSS -->
        <link href="assets/css/jquery.fancybox.min.css" rel="stylesheet"/>
        <!-- Owl Carousel CSS -->
        <link href="assets/css/owl.carousel.min.css" rel="stylesheet"/>
        <link href="assets/css/owl.theme.default.min.css" rel="stylesheet"/>
        <!-- Animate CSS -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>
        <!-- Slick Nav CSS -->
        <link href="assets/css/slicknav.min.css" rel="stylesheet"/>
        <!-- Magnific Popup -->
        <link href="assets/css/magnific-popup.css" rel="stylesheet"/>
        <!-- Learedu Stylesheet -->
        <link href="assets/css/normalize.css" rel="stylesheet"/>
        <link href="assets/style.css" rel="stylesheet"/>
        <link href="assets/css/responsive.css" rel="stylesheet"/>
        <!-- Learedu Color -->
        <link href="assets/css/color/color1.css" rel="stylesheet"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js">
        </script>
    </head>
</html>
<body>
    <div id="app">
        <!-- Book Preloader -->
        <div class="book_preload">
            <div class="book">
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
            </div>
        </div>
        <!--/ End Book Preloader -->
        <!-- Header -->
        <header class="header">
            <!-- Topbar -->
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-12">
                            <!-- Contact -->
                            <ul class="content">
                                <li>
                                    Bienvenido al Programa Integral de Inclusión EHECATL
                                </li>
                            </ul>
                            <!-- End Contact -->
                        </div>
                        <div class="col-lg-4 col-12">
                            <!-- Social -->
                            <ul class="social">
                                @if (Route::has('login'))
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-user">
                                        </i>
                                        Iniciar sesión
                                    </a>
                                </li>
                                @endif
                            </ul>
                            <!-- End Social -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Topbar -->
            <!-- Header Inner -->
            <div class="header-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="logo">
                                <a href="inicio">
                                    <img alt="#" src="assets/images/logo.png">
                                    </img>
                                </a>
                            </div>
                            <div class="mobile-menu">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-12">
                            <!-- Header Widget -->
                            <div class="header-widget">
                                <div class="single-widget">
                                    <i class="fa fa-phone">
                                    </i>
                                    <h4>
                                        Llama ahora
                                        <span>
                                            7711221498
                                        </span>
                                    </h4>
                                </div>
                                <div class="single-widget">
                                    <i class="fa fa-envelope-o">
                                    </i>
                                    <h4>
                                        Enviar mensaje
                                        <a href="mailto:mailus@mail.com">
                                            <span>
                                                reddepadreshuejutla@hotmail.com
                                            </span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="single-widget">
                                    <i class="fa fa-map-marker">
                                    </i>
                                    <h4>
                                        Nuestra ubicación
                                        <span>
                                            <p>
                                                Carretera Pachuca - Huejutla de Reyes
                                            </p>
                                            <p>
                                                El Mirador, c.p. 43000 Huejutla, Hgo.
                                            </p>
                                        </span>
                                    </h4>
                                </div>
                            </div>
                            <!--/ End Header Widget -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Inner -->
            <!-- Header Menu -->
            <div class="header-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="navbar navbar-default">
                                <div class="navbar-collapse">
                                    <!-- Main Menu -->
                                    <ul class="nav menu navbar-nav" id="nav">
                                        <li class="active">
                                            <a href="inicio">
                                                Inicio
                                            </a>
                                        </li>
                                        <li>
                                            <a href="conocenos">
                                                Conócenos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="que_hacemos">
                                                ¿Qué hacemos?
                                            </a>
                                        </li>
                                        <li>
                                            <a href="voluntariado">
                                                ¿Cómo ayudar?
                                            </a>
                                        </li>
                                        <li>
                                            <a href="actualidad">
                                                Actualidad
                                            </a>
                                        </li>
                                        <li>
                                            <a href="contacto">
                                                Contacto
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- End Main Menu -->
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Menu -->
        </header>
        <!-- End Header -->
        <!-- Slider Area -->
        <section class="home-slider">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider overlay" style="background-image:url('assets/images/slider/slider-bg1.jpg')">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-12">
                                <div class="slider-text">
                                    <h1>
                                        Programa de voluntariado
                                        <span>
                                            EHECATL
                                        </span>
                                    </h1>
                                    <p>
                                        La inclusión es la actitud, tendencia o política de integrar a todas las personas en la sociedad, con el objetivo
                                        de que estas puedan participar y contribuir en ella y beneficiarse en este proceso.
                                        <p>
                                            ° Altruista, solidario, amigable son características que posee un voluntario.
                                        </p>
                                        <p>
                                            ° Porque ser voluntario es más que ayudar a quien lo necesita.
                                        </p>
                                        <p>
                                            ° Es regalar tiempo de calidad.
                                        </p>
                                        <p>
                                            ° Es compartir tus experiencias y conocimientos.
                                        </p>
                                        <p>
                                            #YoSoyVoluntario
                                        </p>
                                    </p>
                                    <div class="button">
                                        <a class="btn" href="voluntariado">
                                            Leer más
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ End Single Slider -->
                <!-- Single Slider -->
                <div class="single-slider overlay" data-stellar-background-ratio="0.5" style="background-image:url('assets/images/slider/slider-bg2.jpg')">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-12">
                                <div class="slider-text text-center">
                                    <h1>
                                        Programa Integral de Inclusión
                                        <span>
                                            EHECALTL
                                        </span>
                                    </h1>
                                    <p>
                                        <p>
                                            ° Desarrollar ideas, estrategias y prácticas prometedoras para apoyar y mejorar la educación de los niños con Discapacidad
                                            Múltiple.
                                        </p>
                                        <p>
                                            ° Planear acciones para apoyar y abogar por la educación de los niños con alguna Discapacidad.
                                        </p>
                                    </p>
                                    <div class="button">
                                        {{--
                                        <a class="btn primary" href="#">
                                            Our Courses
                                        </a>
                                        <a class="btn" href="#">
                                            Leer más
                                        </a>
                                        --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ End Single Slider -->
                <!-- Single Slider -->
                <div class="single-slider overlay" data-stellar-background-ratio="0.5" style="background-image:url('assets/images/slider/slider-bg3.jpg')">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-4 col-md-8 offset-md-4 col-12">
                                <div class="slider-text text-right">
                                    <h1>
                                        La
                                        <span>
                                            inclusión:
                                        </span>
                                        ruta social hacia la paz
                                    </h1>
                                    <p>
                                    </p>
                                    {{--
                                    <div class="button">
                                        <a class="btn primary" href="courses.html">
                                            Our Courses
                                        </a>
                                        <a class="btn" href="about.html">
                                            About Learnedu
                                        </a>
                                    </div>
                                    --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ End Single Slider -->
            </div>
        </section>
        <!--/ End Slider Area -->
        <!-- Features -->
        <section class="courses archives section">
            <div class="container">
                <div class="row">
                    <div class="col-12 wow zoomIn">
                        <div class="section-title">
                            <h2>
                                Noticias {{--
                                <span>
                                    Educational
                                </span>
                                Solutions
                            </h2>
                            --}} {{--
                            <p>
                                Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet
                                sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri
                            </p>
                            --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach ($noticias as $noticias)
                    <div class="col-lg-4 col-md-4 col-12 wow fadeInUp" data-wow-delay="0.8s">
                        <!-- Single Course -->
                        <div class="single-course">
                            <div class="course-head overlay">
                                <img alt="#" src="assets/images/noticias/{{ $noticias->vchimagen }}">
                                    <a class="btn" href="/{{ $noticias->vchtitulo }}">
                                        <i class="fa fa-link">
                                        </i>
                                    </a>
                                </img>
                            </div>
                            <div class="single-content">
                                <h4>
                                    <a href="noticia/{{ $noticias->vchtitulo }}">
                                        {{ $noticias->vchtitulo }}
                                    </a>
                                </h4>
                                <p>
                                    {{ $noticias->vchparrafo1 }}...
                                    <a href="#">
                                        Ver más
                                    </a>
                                </p>
                            </div>
                            <div class="course-meta">
                                <div class="meta-left">
                                    {{--
                                    <span>
                                        <i class="fa fa-clock-o">
                                        </i>
                                        {{ $noticias->vchfecha }}
                                    </span>
                                    --}}
                                </div>
                                <span class="price">
                                    <i class="fa fa-clock-o">
                                    </i>
                                    {{ $noticias->vchfecha }}
                                </span>
                            </div>
                        </div>
                        <!--/ End Single Course -->
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Features -->
        <!-- Courses -->
        <section class="courses section-bg section">
            <div class="container">
                <div class="row">
                    <div class="col-12 wow zoomIn">
                        <div class="section-title">
                            <h2>
                                ¿ Cómo
                                <span>
                                    ayudar
                                </span>
                                ?
                            </h2>
                            {{--
                            <p>
                                Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet
                                sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri
                            </p>
                            --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="course-slider">
                            <!-- Single Course -->
                            <div class="single-course">
                                <div class="course-head overlay">
                                    <img alt="#" src="assets/images/programa_voluntariado.jpg">
                                        <a class="btn" href="voluntariado">
                                            <i class="fa fa-link">
                                            </i>
                                        </a>
                                    </img>
                                </div>
                                <div class="single-content">
                                    <h4>
                                        <a href="voluntariado">
                                            Programa de voluntariado
                                        </a>
                                    </h4>
                                    <p>
                                        El programa del voluntariado de la Red de Padres de Hijos con Discapacidad es una ventana abierta a la inclusión
                                        de los niños...
                                        <a href="voluntariado">
                                            Ver más
                                        </a>
                                    </p>
                                </div>
                                <div class="course-meta">
                                    <div style="text-align:center;">
                                        <span class="price">
                                            INFÓRMATE
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!--/ End Single Course -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ End Courses -->
        <!-- Fun Facts -->
        <div class="fun-facts overlay" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="0.4s">
                        <!-- Single Fact -->
                        <div class="single-fact">
                            <i class="fa fa-calendar-plus-o">
                            </i>
                            <div class="number">
                                <span class="counter">
                                    80
                                </span>
                                k+
                            </div>
                            <p>
                                Próximas actividades
                            </p>
                        </div>
                        <!--/ End Single Fact -->
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="0.6s">
                        <!-- Single Fact -->
                        <div class="single-fact">
                            <i class="fa fa-users">
                            </i>
                            <div class="number">
                                <span class="counter">
                                    10
                                </span>
                            </div>
                            <p>
                                Voluntarios activos
                            </p>
                        </div>
                        <!--/ End Single Fact -->
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="0.8s">
                        <!-- Single Fact -->
                        <div class="single-fact">
                            <i class="fa fa-video-camera">
                            </i>
                            <div class="number">
                                <span class="counter">
                                    278
                                </span>
                            </div>
                            <p>
                                Video Cources
                            </p>
                        </div>
                        <!--/ End Single Fact -->
                    </div>
                    <div class="col-lg-3 col-md-6 col-6 wow zoomIn" data-wow-delay="1s">
                        <!-- Single Fact -->
                        <div class="single-fact">
                            <i class="fa fa-trophy">
                            </i>
                            <div class="number">
                                <span class="counter">
                                    308
                                </span>
                                +
                            </div>
                            <p>
                                Awards Won
                            </p>
                        </div>
                        <!--/ End Single Fact -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer class="footer overlay section wow fadeIn">
            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="bottom-head">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Social -->
                                        <ul class="social">
                                            <li>
                                                <a href="https://twitter.com/?lang=es" target="_blank">
                                                    <i class="fa fa-twitter">
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://es-la.facebook.com/" target="_blank">
                                                    <i class="fa fa-facebook">
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.youtube.com/?hl=es-419&gl=MX" target="_blank">
                                                    <i class="fa fa-youtube">
                                                    </i>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- End Social -->
                                        <!-- Copyright -->
                                        <div class="copyright">
                                            <p>
                                                © Copyright 2018. Programa Integral de Inclusión
                                                <a href="#">
                                                    EHECATL
                                                </a>
                                                . Todos los derechos reservados.
                                            </p>
                                        </div>
                                        <!--/ End Copyright -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Footer Bottom -->
        </footer>
        <!--/ End Footer -->
    </div>
    <script src="assets/js/jquery.min.js">
    </script>
    <script src="assets/js/jquery-migrate.min.js">
    </script>
    <!-- Popper JS-->
    <script src="assets/js/popper.min.js">
    </script>
    <!-- Bootstrap JS-->
    <script src="assets/js/bootstrap.min.js">
    </script>
    <!-- Colors JS-->
    <script src="assets/js/colors.js">
    </script>
    <!-- Jquery Steller JS -->
    <script src="assets/js/jquery.stellar.min.js">
    </script>
    <!-- Particle JS -->
    <script src="assets/js/particles.min.js">
    </script>
    <!-- Fancy Box JS-->
    <script src="assets/js/facnybox.min.js">
    </script>
    <!-- Magnific Popup JS-->
    <script src="assets/js/jquery.magnific-popup.min.js">
    </script>
    <!-- Masonry JS-->
    <script src="assets/js/masonry.pkgd.min.js">
    </script>
    <!-- Circle Progress JS -->
    <script src="assets/js/circle-progress.min.js">
    </script>
    <!-- Owl Carousel JS-->
    <script src="assets/js/owl.carousel.min.js">
    </script>
    <!-- Waypoints JS-->
    <script src="assets/js/waypoints.min.js">
    </script>
    <!-- Slick Nav JS-->
    <script src="assets/js/slicknav.min.js">
    </script>
    <!-- Counter Up JS -->
    <script src="assets/js/jquery.counterup.min.js">
    </script>
    <!-- Easing JS-->
    <script src="assets/js/easing.min.js">
    </script>
    <!-- Wow Min JS-->
    <script src="assets/js/wow.min.js">
    </script>
    <!-- Scroll Up JS-->
    <script src="assets/js/jquery.scrollUp.min.js">
    </script>
    <!-- Google Maps JS -->
    <script src="assets/js/gmaps.min.js">
    </script>
    <!-- Main JS-->
    <script src="assets/js/main.js">
    </script>
    <!-- sweetalert -->
    <script src="js/sweetalert.min.js">
    </script>
</body>
