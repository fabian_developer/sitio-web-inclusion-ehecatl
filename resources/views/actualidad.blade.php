<!DOCTYPE html>
<html>
    {{-- class="no-js" lang="zxx" --}} {{-- lang="{{ app()->getLocale() }}" --}}
    <head>
        <!-- Meta -->
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="SITE KEYWORDS HERE" name="keywords"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <!-- Title -->
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <!-- Favicon -->
        <link href="assets/images/icono.png" rel="icon" type="image/png"/>
        <!-- Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"/>
        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Font Awesome CSS -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
        <!-- Fancy Box CSS -->
        <link href="assets/css/jquery.fancybox.min.css" rel="stylesheet"/>
        <!-- Owl Carousel CSS -->
        <link href="assets/css/owl.carousel.min.css" rel="stylesheet"/>
        <link href="assets/css/owl.theme.default.min.css" rel="stylesheet"/>
        <!-- Animate CSS -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>
        <!-- Slick Nav CSS -->
        <link href="assets/css/slicknav.min.css" rel="stylesheet"/>
        <!-- Magnific Popup -->
        <link href="assets/css/magnific-popup.css" rel="stylesheet"/>
        <!-- Learedu Stylesheet -->
        <link href="assets/css/normalize.css" rel="stylesheet"/>
        <link href="assets/style.css" rel="stylesheet"/>
        <link href="assets/css/responsive.css" rel="stylesheet"/>
        <!-- Learedu Color -->
        <link href="assets/css/color/color1.css" rel="stylesheet"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js">
        </script>
    </head>
</html>
<body>
    <div id="app">
        <!-- Book Preloader -->
        <div class="book_preload">
            <div class="book">
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
            </div>
        </div>
        <!--/ End Book Preloader -->
        <!-- Header -->
        <header class="header">
            <!-- Topbar -->
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-12">
                            <!-- Contact -->
                            <ul class="content">
                                <li>
                                    Bienvenido al Programa Integral de Inclusión EHECATL
                                </li>
                            </ul>
                            <!-- End Contact -->
                        </div>
                        <div class="col-lg-4 col-12">
                            <!-- Social -->
                            <ul class="social">
                                @if (Route::has('login'))
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-user">
                                        </i>
                                        Iniciar sesión
                                    </a>
                                </li>
                                @endif
                            </ul>
                            <!-- End Social -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Topbar -->
            <!-- Header Inner -->
            <div class="header-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="logo">
                                <a href="index.html">
                                    <img alt="#" src="assets/images/logo.png">
                                    </img>
                                </a>
                            </div>
                            <div class="mobile-menu">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-12">
                            <!-- Header Widget -->
                            <div class="header-widget">
                                <div class="single-widget">
                                    <i class="fa fa-phone">
                                    </i>
                                    <h4>
                                        Llama ahora
                                        <span>
                                            7711221498
                                        </span>
                                    </h4>
                                </div>
                                <div class="single-widget">
                                    <i class="fa fa-envelope-o">
                                    </i>
                                    <h4>
                                        Enviar mensaje
                                        <a href="mailto:mailus@mail.com">
                                            <span>
                                                reddepadreshuejutla@hotmail.com
                                            </span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="single-widget">
                                    <i class="fa fa-map-marker">
                                    </i>
                                    <h4>
                                        Nuestra ubicación
                                        <span>
                                            <p>
                                                Carretera Pachuca - Huejutla de Reyes
                                            </p>
                                            <p>
                                                El Mirador, c.p. 43000 Huejutla, Hgo.
                                            </p>
                                        </span>
                                    </h4>
                                </div>
                            </div>
                            <!--/ End Header Widget -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Inner -->
            <!-- Header Menu -->
            <div class="header-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="navbar navbar-default">
                                <div class="navbar-collapse">
                                    <!-- Main Menu -->
                                    <ul class="nav menu navbar-nav" id="nav">
                                        <li class="">
                                            <a href="inicio">
                                                Inicio
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="conocenos">
                                                Conócenos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="que_hacemos">
                                                ¿Qué hacemos?
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="voluntariado">
                                                ¿Cómo ayudar?
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="actualidad">
                                                Actualidad
                                            </a>
                                        </li>
                                        <li>
                                            <a href="contacto">
                                                Contacto
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Menu -->
        </header>
        <!-- End Header -->
        <!-- Start Breadcrumbs -->
        <section class="breadcrumbs overlay">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>
                            Actualidad
                        </h2>
                    </div>
                </div>
            </div>
        </section>
        <!--/ End Breadcrumbs -->
        {{--  --}}
        <!-- Features -->
        <section class="courses archives section">
            <div class="container">
                <div class="row">
                    @foreach ($noticias as $noticia)
                    <div class="col-lg-4 col-md-4 col-12 wow fadeInUp" data-wow-delay="0.8s">
                        <!-- Single Course -->
                        <div class="single-course">
                            <div class="course-head overlay">
                                <img alt="#" src="assets/images/noticias/{{ $noticia->vchimagen }}">
                                    <a class="btn" href="/noticia/{{ $noticia->vchtitulo }}">
                                        <i class="fa fa-link">
                                        </i>
                                    </a>
                                </img>
                            </div>
                            <div class="single-content">
                                <h4>
                                    <a href="/noticia/{{ $noticia->vchtitulo }}">
                                        {{ $noticia->vchtitulo }}
                                    </a>
                                </h4>
                                <p>
                                    {{ $noticia->vchparrafo1 }}...
                                    <a href="#">
                                        Ver más
                                    </a>
                                </p>
                            </div>
                            <div class="course-meta">
                                <div class="meta-left">
                                    {{--
                                    <span>
                                        <i class="fa fa-clock-o">
                                        </i>
                                        {{ $noticias->vchfecha }}
                                    </span>
                                    --}}
                                </div>
                                <span class="price">
                                    <i class="fa fa-clock-o">
                                    </i>
                                    {{ $noticia->vchfecha }}
                                </span>
                            </div>
                        </div>
                        <!--/ End Single Course -->
                    </div>
                    @endforeach
                </div>
            </div>
            <div style="text-align: center">
                <ul class="pagination">
                    {{ $noticias->links() }}
                </ul>
            </div>
        </section>
        <!-- End Features -->
        <!-- Footer -->
        <footer class="footer overlay section wow fadeIn">
            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="bottom-head">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Social -->
                                        <ul class="social">
                                            <li>
                                                <a href="https://twitter.com/?lang=es" target="_blank">
                                                    <i class="fa fa-twitter">
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://es-la.facebook.com/" target="_blank">
                                                    <i class="fa fa-facebook">
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.youtube.com/?hl=es-419&gl=MX" target="_blank">
                                                    <i class="fa fa-youtube">
                                                    </i>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- End Social -->
                                        <!-- Copyright -->
                                        <div class="copyright">
                                            <p>
                                                © Copyright 2018. Programa Integral de Inclusión
                                                <a href="#">
                                                    EHECATL
                                                </a>
                                                . Todos los derechos reservados.
                                            </p>
                                        </div>
                                        <!--/ End Copyright -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Footer Bottom -->
        </footer>
        <!--/ End Footer -->
    </div>
    <script src="assets/js/jquery.min.js">
    </script>
    <script src="assets/js/jquery-migrate.min.js">
    </script>
    <!-- Popper JS-->
    <script src="assets/js/popper.min.js">
    </script>
    <!-- Bootstrap JS-->
    <script src="assets/js/bootstrap.min.js">
    </script>
    <!-- Colors JS-->
    <script src="assets/js/colors.js">
    </script>
    <!-- Jquery Steller JS -->
    <script src="assets/js/jquery.stellar.min.js">
    </script>
    <!-- Particle JS -->
    <script src="assets/js/particles.min.js">
    </script>
    <!-- Fancy Box JS-->
    <script src="assets/js/facnybox.min.js">
    </script>
    <!-- Magnific Popup JS-->
    <script src="assets/js/jquery.magnific-popup.min.js">
    </script>
    <!-- Masonry JS-->
    <script src="assets/js/masonry.pkgd.min.js">
    </script>
    <!-- Circle Progress JS -->
    <script src="assets/js/circle-progress.min.js">
    </script>
    <!-- Owl Carousel JS-->
    <script src="assets/js/owl.carousel.min.js">
    </script>
    <!-- Waypoints JS-->
    <script src="assets/js/waypoints.min.js">
    </script>
    <!-- Slick Nav JS-->
    <script src="assets/js/slicknav.min.js">
    </script>
    <!-- Counter Up JS -->
    <script src="assets/js/jquery.counterup.min.js">
    </script>
    <!-- Easing JS-->
    <script src="assets/js/easing.min.js">
    </script>
    <!-- Wow Min JS-->
    <script src="assets/js/wow.min.js">
    </script>
    <!-- Scroll Up JS-->
    <script src="assets/js/jquery.scrollUp.min.js">
    </script>
    <!-- Google Maps JS -->
    {{--
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyC0RqLa90WDfoJedoE3Z_Gy7a7o8PCL2jw">
    </script>
    --}}
    <script src="assets/js/gmaps.min.js">
    </script>
    <!-- Main JS-->
    <script src="assets/js/main.js">
    </script>
    <!-- Voluntariado -->
    <script src="assets/voluntariado.js">
    </script>
    <!-- sweetalert -->
    <script src="js/sweetalert.min.js">
    </script>
</body>
