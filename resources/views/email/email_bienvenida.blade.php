<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <style media="screen" type="text/css">
            /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #333333;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #fafafa;
      font-size: 24px;
      color: #ffffff;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #eeeeee;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #ffffff;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #ffffff;
    }

    .force-full-width {
      width: 100% !important;
    }
        </style>
        <style media="only screen and (max-width: 600px)" type="text/css">
            @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
        </style>
    </head>
    <body bgcolor="" class="background" leftmargin="0" marginheight="0" marginwidth="0" offset="0" topmargin="0">
        <table align="center" border="0" cellpadding="0" cellspacing="0" class="background" height="100%" width="100%">
            <tr>
                <td align="center" class="background" valign="top" width="100%">
                    <center>
                        <table cellpadding="0" cellspacing="0" class="wrap" width="600">
                            <tr>
                                <td class="wrap-cell" style="padding-top:30px; padding-bottom:30px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" class="force-full-width">
                                        <tr>
                                            <td class="header-cell" height="80" style="text-align:center;" valign="top">
                                                <img alt="logo" height="100" src="http://portal-integral-de-inclusion-ehecatl.proyectos-pyme.com.mx/assets/images/logo_login.png" width="260">
                                                </img>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="body-cell" valign="top">
                                                <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="padding-bottom:20px; background-color:#ffffff;" valign="top">
                                                            Hola {{ $nombre }},
                                                            <br>
                                                                <br>
                                                                    La Red de Padres de Hijos con Discapacidad en la Región Huejutla te agradece por formar parte del programa de voluntariado.
                                                                    <br/>
                                                                    Con los siguientes datos puede iniciar sesión y poder agendar actividades para realizarlas con los niños.
                                                                </br>
                                                            </br>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="center" style="padding:20px 0;">
                                                                        <center>
                                                                            <table cellpadding="0" cellspacing="0" class="card">
                                                                                <tr>
                                                                                    <td style="background-color:orange; text-align:center; padding:10px; color:white; ">
                                                                                        Datos para iniciar sesión
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="border:1px solid orange;">
                                                                                        <table cellpadding="20" cellspacing="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td class="data-heading" width="150">
                                                                                                                Correo electrónico:
                                                                                                            </td>
                                                                                                            <td class="data-value">
                                                                                                                {{ $correo }}
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td class="data-heading" width="150">
                                                                                                                Contraseña:
                                                                                                            </td>
                                                                                                            <td class="data-value">
                                                                                                                {{ $password }}
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top:20px;background-color:#ffffff;">
                                                            <br/>
                                                            ¡Gracias!
                                                            <br>
                                                                Programa Integral de Inclusión EHECATL
                                                            </br>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="footer-cell" valign="top">
                                                © Copyright 2018. Programa Integral de Inclusión EHECATL. Todos los derechos reservados.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </center>
                </td>
            </tr>
        </table>
    </body>
</html>
