<!DOCTYPE html>
{{-- class="no-js" lang="zxx" --}} {{-- lang="{{ app()->getLocale() }}" --}}
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Meta -->
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <meta content="SITE KEYWORDS HERE" name="keywords"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <!-- Title -->
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <!-- Favicon -->
        <link href="assets/images/icono.png" rel="icon" type="image/png"/>
        <!-- Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"/>
        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Font Awesome CSS -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet"/>
        <!-- Fancy Box CSS -->
        <link href="assets/css/jquery.fancybox.min.css" rel="stylesheet"/>
        <!-- Owl Carousel CSS -->
        <link href="assets/css/owl.carousel.min.css" rel="stylesheet"/>
        <link href="assets/css/owl.theme.default.min.css" rel="stylesheet"/>
        <!-- Animate CSS -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>
        <!-- Slick Nav CSS -->
        <link href="assets/css/slicknav.min.css" rel="stylesheet"/>
        <!-- Magnific Popup -->
        <link href="assets/css/magnific-popup.css" rel="stylesheet"/>
        <!-- Learedu Stylesheet -->
        <link href="assets/css/normalize.css" rel="stylesheet"/>
        <link href="assets/style.css" rel="stylesheet"/>
        <link href="assets/css/responsive.css" rel="stylesheet"/>
        <!-- Learedu Color -->
        <link href="assets/css/color/color1.css" rel="stylesheet"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js">
        </script>
    </head>
</html>
<body>
    <div id="app">
        <!-- Book Preloader -->
        <div class="book_preload">
            <div class="book">
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
            </div>
        </div>
        <!--/ End Book Preloader -->
        <!-- Header -->
        <header class="header">
            <!-- Topbar -->
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-12">
                            <!-- Contact -->
                            <ul class="content">
                                <li>
                                    Bienvenido al Programa Integral de Inclusión EHECATL
                                </li>
                            </ul>
                            <!-- End Contact -->
                        </div>
                        <div class="col-lg-4 col-12">
                            <!-- Social -->
                            <ul class="social">
                                @if (Route::has('login'))
                                <li>
                                    <a href="{{ route('login') }}">
                                        <i class="fa fa-user">
                                        </i>
                                        Iniciar sesión
                                    </a>
                                </li>
                                @endif
                            </ul>
                            <!-- End Social -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Topbar -->
            <!-- Header Inner -->
            <div class="header-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-12">
                            <div class="logo">
                                <a href="index.html">
                                    <img alt="#" src="assets/images/logo.png">
                                    </img>
                                </a>
                            </div>
                            <div class="mobile-menu">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-12">
                            <!-- Header Widget -->
                            <div class="header-widget">
                                <div class="single-widget">
                                    <i class="fa fa-phone">
                                    </i>
                                    <h4>
                                        Llama ahora
                                        <span>
                                            7711221498
                                        </span>
                                    </h4>
                                </div>
                                <div class="single-widget">
                                    <i class="fa fa-envelope-o">
                                    </i>
                                    <h4>
                                        Enviar mensaje
                                        <a href="mailto:mailus@mail.com">
                                            <span>
                                                reddepadreshuejutla@hotmail.com
                                            </span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="single-widget">
                                    <i class="fa fa-map-marker">
                                    </i>
                                    <h4>
                                        Nuestra ubicación
                                        <span>
                                            <p>
                                                Carretera Pachuca - Huejutla de Reyes
                                            </p>
                                            <p>
                                                El Mirador, c.p. 43000 Huejutla, Hgo.
                                            </p>
                                        </span>
                                    </h4>
                                </div>
                            </div>
                            <!--/ End Header Widget -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Inner -->
            <!-- Header Menu -->
            <div class="header-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="navbar navbar-default">
                                <div class="navbar-collapse">
                                    <!-- Main Menu -->
                                    <ul class="nav menu navbar-nav" id="nav">
                                        <li class="">
                                            <a href="inicio">
                                                Inicio
                                            </a>
                                        </li>
                                        <li>
                                            <a href="quienes_somos">
                                                Conócenos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="que_hacemos">
                                                ¿Qué hacemos?
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="voluntariado">
                                                ¿Cómo ayudar?
                                            </a>
                                        </li>
                                        <li>
                                            <a href="actualidad">
                                                Actualidad
                                            </a>
                                        </li>
                                        <li>
                                            <a href="contacto">
                                                Contacto
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Header Menu -->
        </header>
        <!-- End Header -->
        <!-- Start Breadcrumbs -->
        <section class="breadcrumbs overlay">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>
                            Programa de voluntariado
                        </h2>
                    </div>
                </div>
            </div>
        </section>
        <!--/ End Breadcrumbs -->
        <div id="informacion">
            <!-- Faqs -->
            <section class="faq page section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title">
                                <h2>
                                    ¿Qué es el programa de
                                    <span>
                                        voluntariado
                                    </span>
                                    ?
                                </h2>
                                <p>
                                    El programa del voluntariado de la Red de Padres de Hijos con Discapacidad es una ventana abierta a la inclusión de los niños, ya que por medio de éste, los niños pueden ser incluidos en la sociedad, realizando por medio
                                    de voluntarios altruistas, instituciones educativas, sector empresarial, asociaciones civiles etc., actividades recreativas, visitas guiadas a instituciones educativas y/o empresas con el objetivo de fomentar la inclusión
                                    social-educativa de los niños con discapacidad, ya que les permitirá desarrollarse como personas en un ambiente acorde a sus propias condiciones, disminuyendo las barreras para el aprendizaje y participando activamente
                                    en la sociedad. Debido a lo anterior se han elaborado las reglas de operación que definen las características que debe cumplir un VOLUNTARIO, así como los requisitos para poder ser parte de este programa.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title">
                                {{--
                                <h2>
                                    Requisitos para ser
                                    <span>
                                        voluntario
                                    </span>
                                </h2>
                                --}}
                                <h2>
                                    Sé
                                    <span>
                                        voluntario
                                    </span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1 col-12">
                            <div class="faq-content">
                                <div aria-multiselectable="true" class="panel-group" id="accordion" role="tablist">
                                    <div class="panel panel-default">
                                        <!-- Single Faq -->
                                        <div class="faq-heading" id="FaqTitle1">
                                            <h4 class="faq-title">
                                                <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#faq1">
                                                    ¿Por qué ser voluntario?
                                                </a>
                                            </h4>
                                        </div>
                                        <div aria-labelledby="FaqTitle1" class="panel-collapse collapse" id="faq1" role="tabpanel">
                                            <div class="faq-body">
                                                Las razones principales por las cuales se puede incluir y participar en este programa son:
                                                <br>
                                                    <li type="square">
                                                        Eres una persona altruista
                                                        <br>
                                                            <li type="square">
                                                                Le gusta convivir con personas
                                                                <br>
                                                                    <li type="square">
                                                                        Ayuda a las personas que lo necesitan
                                                                        <br>
                                                                            <li type="square">
                                                                                Poseé un espíritu de solidaridad
                                                                                <br>
                                                                                    <li type="square">
                                                                                        Practica el valor de empatía
                                                                                        <br>
                                                                                            <li type="square">
                                                                                                Respeta y defiende activamente la dignidad de las personas en condiciones de vulnerabilidad
                                                                                                <br>
                                                                                                    <li type="square">
                                                                                                        Desea potenciar el desarrollo integral de las personas en condiciones diferentes
                                                                                                        <br>
                                                                                                            <li type="square">
                                                                                                                Es responsable y amigable
                                                                                                            </li>
                                                                                                        </br>
                                                                                                    </li>
                                                                                                </br>
                                                                                            </li>
                                                                                        </br>
                                                                                    </li>
                                                                                </br>
                                                                            </li>
                                                                        </br>
                                                                    </li>
                                                                </br>
                                                            </li>
                                                        </br>
                                                    </li>
                                                </br>
                                            </div>
                                        </div>
                                        <!--/ End Single Faq -->
                                    </div>
                                    <div class="panel panel-default">
                                        <!-- Single Faq -->
                                        <div class="faq-heading" id="FaqTitle2">
                                            <h4 class="faq-title">
                                                <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#faq2">
                                                    Perfil del voluntariado
                                                </a>
                                            </h4>
                                        </div>
                                        <div aria-labelledby="FaqTitle2" class="panel-collapse collapse show" id="faq2" role="tabpanel">
                                            <div class="faq-body">
                                                La persona que desee formar parte del Voluntariado de la Red de Padres de Hijos con Discapacidad deberá reunir el mayor número posible de los requisitos que se indican a continuación:
                                                <br>
                                                    <li type="square">
                                                        Ser mayor de dieciocho años, sin importar sexo, estado civil, raza, condición social o económica, etc., pero es importante su impacto sobre las personas: apariencia, aseo, conversación, modales,
                                                etc.
                                                        <br>
                                                            <li type="square">
                                                                Disponibilidad de tiempo para llevar a cabo las actividades programadas con los niños y niñas con discapacidad.
                                                                <br>
                                                                    <li type="square">
                                                                        Poseer espíritu de solidaridad y de entrega generosa de lo mejor de uno mismo, respetar y defender activamente la dignidad de las personas y potenciar el desarrollo integral de los niños, con empatía
                                                y paciencia.
                                                                        <br>
                                                                            <li type="square">
                                                                                Poseer madurez, equilibrio emocional, capacidad de convivencia, adaptación al trabajo en equipo, un alto concepto ético en su escala de valores, criterios de flexibilidad, saber dar un trato amable
                                                a los niños con discapacidad.
                                                                                <br>
                                                                                    <li type="square">
                                                                                        Facilitar la integración, formación y participación de sus compañeros voluntarios y crear lazos de unión entre todos para potencializar la actividad que se lleve a cabo y se cumplan los objetivos
                                                                                        <br>
                                                                                            <li type="square">
                                                                                                Transmitir con sus actividades, acciones y palabras, aquellos valores e ideales que persigue con su trabajo como voluntario y que habrán de coincidir con los fines y valores del Programa del Voluntariado,
                                                respetando la Organización sin utilizarla en beneficio propio.
                                                                                                <br>
                                                                                                    <li type="square">
                                                                                                        Ser participativo y comunicativo en las actividades programadas con los niños con discapacidad
                                                                                                        <br>
                                                                                                            <li type="square">
                                                                                                                El voluntario debe ser una persona responsable, amigable, y flexible.
                                                                                                                <br>
                                                                                                                    <li type="square">
                                                                                                                        El voluntario debe tener el gusto y agrado para trabajar con los niños con discapacidad y entender su condición actual.
                                                                                                                        <br>
                                                                                                                            <li type="square">
                                                                                                                                El voluntario debe de sentir una profunda obligación como ciudadano de apoyar y ayudar a las escuelas de educación especial y servicios de apoyo a las escuelas regulares en su esfuerzo de educar
                                                a cada niño con discapacidad.
                                                                                                                            </li>
                                                                                                                        </br>
                                                                                                                    </li>
                                                                                                                </br>
                                                                                                            </li>
                                                                                                        </br>
                                                                                                    </li>
                                                                                                </br>
                                                                                            </li>
                                                                                        </br>
                                                                                    </li>
                                                                                </br>
                                                                            </li>
                                                                        </br>
                                                                    </li>
                                                                </br>
                                                            </li>
                                                        </br>
                                                    </li>
                                                </br>
                                            </div>
                                        </div>
                                        <!--/ End Single Faq -->
                                    </div>
                                    <div class="panel panel-default">
                                        <!-- Single Faq -->
                                        <div class="faq-heading" id="FaqTitle3">
                                            <h4 class="faq-title">
                                                <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#faq3">
                                                    Requisitos
                                                </a>
                                            </h4>
                                        </div>
                                        <div aria-labelledby="FaqTitle3" class="panel-collapse collapse" id="faq3" role="tabpanel">
                                            <div class="faq-body">
                                                <br>
                                                    <li type="square">
                                                        Cedula de inscripción (Requisitarla)
                                                        <br>
                                                            <li type="square">
                                                                Una copia de identificación oficial.
                                                                <br>
                                                                    <li type="square">
                                                                        Una copia de CURP.
                                                                        <br>
                                                                            <li type="square">
                                                                                Una copia de comprobante de domicilio actual.
                                                                                <br>
                                                                                    <li type="square">
                                                                                        Dos fotografías tamaño infantil (recientes, a color o en blanco y negro).
                                                                                    </li>
                                                                                </br>
                                                                            </li>
                                                                        </br>
                                                                    </li>
                                                                </br>
                                                            </li>
                                                        </br>
                                                    </li>
                                                </br>
                                            </div>
                                        </div>
                                        <!--/ End Single Faq -->
                                    </div>
                                    <div class="panel panel-default">
                                        <!-- Single Faq -->
                                        <div class="faq-heading" id="FaqTitle4">
                                            <h4 class="faq-title">
                                                <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#faq4">
                                                    Propuesta o idea de algunas actividades a desarrollar
                                                </a>
                                            </h4>
                                        </div>
                                        <div aria-labelledby="FaqTitle4" class="panel-collapse collapse" id="faq4" role="tabpanel">
                                            <div class="faq-body">
                                                <br>
                                                    <li type="square">
                                                        <strong>
                                                            Visitas guiadas a instituciones educativas y /o empresas
                                                        </strong>
                                                        <br>
                                                            Los niños podrán asistir a instituciones educativas en donde se podrían organizar distintas actividades como por ejemplo juegos organizados, recorrido a las distintas áreas que la conforman, prácticas
                                                y/o talleres, etc., siempre estarán guiados por los voluntarios de dicha institución.
                                                            <br>
                                                                <li type="square">
                                                                    <strong>
                                                                        Salida al cine
                                                                    </strong>
                                                                    <br>
                                                                        Para salida se llevaran a cierta cantidad de niños que se especificara en la cedula llenada anteriormente los niños podrán observar la película de su agrado es importante que el voluntario este al pendiente
                                                de cada uno de ellos desde el momento en que los recoge en el lugar acordado hasta finalizar la actividad.
                                                                        <br>
                                                                            <li type="square">
                                                                                <strong>
                                                                                    Salida a la pizzería
                                                                                </strong>
                                                                                <br>
                                                                                    Antes de llevar acabo la actividad es necesario considerar el estado alimenticio de cada uno de los niños que se llevaran ya que no todos pueden consumir alimentos que contengan harina y sus derivados.
                                                Una vez hecha está observación se deberá indicar el lugar en donde se llevara a cabo posteriormente el voluntario será responsable de llevarlos y traerlos en el tiempo acordado.
                                                                                    <br>
                                                                                        <li type="square">
                                                                                            <strong>
                                                                                                Visita al parque ecológico
                                                                                            </strong>
                                                                                            <br>
                                                                                                Se puede optar por la visita al parque ecológico, en donde los niños podrán interactuar con la naturaleza.
                                                                                                <br>
                                                                                                    <li type="square">
                                                                                                        <strong>
                                                                                                            Encuentro deportivos
                                                                                                        </strong>
                                                                                                        <br>
                                                                                                            Es posible llevar a los niños a observar encuentros deportivos, esto favorece la interaccion con otras personas y ayuda a que socialicen.
                                                                                                            <br>
                                                                                                                <li type="square">
                                                                                                                    <strong>
                                                                                                                        Un día en familia
                                                                                                                    </strong>
                                                                                                                    <br>
                                                                                                                        Es posible realizar un día de campo en el que los padres sean partícipes y junto con el voluntario puedan divertirse y convivir.
                                                                                                                        <br>
                                                                                                                            <li type="square">
                                                                                                                                <strong>
                                                                                                                                    Fiestas de cumpleaños
                                                                                                                                </strong>
                                                                                                                                <br>
                                                                                                                                    Se puede llevar a los chicos a cumpleaños de sus amigos y favorecer la convivencia sana.
                                                                                                                                    <br>
                                                                                                                                        <li type="square">
                                                                                                                                            <strong>
                                                                                                                                                Recorrido por la catedral de Huejutla
                                                                                                                                            </strong>
                                                                                                                                            <br>
                                                                                                                                                El voluntario tiene la opción de llevar a los niños a un recorrido por la catedral de Huejutla y platicarles un poco sobre la historia de la misma.
                                                                                                                                            </br>
                                                                                                                                        </li>
                                                                                                                                    </br>
                                                                                                                                </br>
                                                                                                                            </li>
                                                                                                                        </br>
                                                                                                                    </br>
                                                                                                                </li>
                                                                                                            </br>
                                                                                                        </br>
                                                                                                    </li>
                                                                                                </br>
                                                                                            </br>
                                                                                        </li>
                                                                                    </br>
                                                                                </br>
                                                                            </li>
                                                                        </br>
                                                                    </br>
                                                                </li>
                                                            </br>
                                                        </br>
                                                    </li>
                                                </br>
                                            </div>
                                        </div>
                                        <!--/ End Single Faq -->
                                    </div>
                                    <div class="panel panel-default">
                                        <!-- Single Faq -->
                                        <div class="faq-heading" id="FaqTitle5">
                                            <h4 class="faq-title">
                                                <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#faq5">
                                                    Beneficios de ser voluntario
                                                </a>
                                            </h4>
                                        </div>
                                        <div aria-labelledby="FaqTitle5" class="panel-collapse collapse" id="faq5" role="tabpanel">
                                            <div class="faq-body">
                                                <br>
                                                    Ser voluntario te da la oportunidad de practicar y desarrollar tus habilidades sociales con las personas en condiciones de vulnerabilidad, participar como voluntario es una forma social y humanitaria
                                                porque aportarás un gran valor a los demás, pero también a uno mismo.
                                                    <br>
                                                        Mediante el voluntariado nos ayudamos a nosotros mismos mientras ayudamos a otros. También brinda la posibilidad de pertenecer a un grupo que promueve la transformación social a favor de las personas
                                                con discapacidad y de sus familias.
                                                    </br>
                                                </br>
                                            </div>
                                        </div>
                                        <!--/ End Single Faq -->
                                    </div>
                                    <div class="panel panel-default">
                                        <!-- Single Faq -->
                                        <div class="faq-heading" id="FaqTitle6">
                                            <h4 class="faq-title">
                                                <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#faq6">
                                                    ¿Cuál sería mi aportación a la educación de los niños con discapacidad al
                                                    ser voluntario?
                                                </a>
                                            </h4>
                                        </div>
                                        <div aria-labelledby="FaqTitle6" class="panel-collapse collapse" id="faq6" role="tabpanel">
                                            <div class="faq-body">
                                                Poder aportar ideas, experiencias vitales, fomentar la autodeterminación y capacidades de convivencia del niño, facilitando su inclusión social-educativa. Diversos conocimientos que poseas de tu comunidad, carrera
                                                o recursos. Gracias a esto podrás difundir la misión y valores de la Red de Padres de Hijos con Discapacidad además de animar a otros a participar como voluntarios.
                                            </div>
                                        </div>
                                        <!--/ End Single Faq -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row contact">
                        <div class="col-lg-12 col-md-6 col-12">
                            <div class="form-head">
                                <!-- Form -->
                                <br>
                                    <div class="form-group" style="text-align:center;">
                                        <div class="button">
                                            {{--
                                            <a href="registro_voluntariado">
                                                --}}
                                                <button class="btn primary" id="btnvoluntariado" name="btnvoluntariado" type="button">
                                                    Quiero ser voluntario
                                                </button>
                                                {{--
                                            </a>
                                            --}}
                                        </div>
                                    </div>
                                    <!--/ End Form -->
                                </br>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ End Faqs -->
            <!-- Testimonials -->
            <section class="testimonials no-bg overlay section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h2>
                                    <span>
                                        Testimonios
                                    </span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="testimonial-slider">
                                @foreach ($testimonio as $testimonio)
                                <!-- Single Testimonial -->
                                <div class="single-testimonial">
                                    <img alt="#" src="voluntario/foto/{{ $testimonio->vchfoto_perfil}}">
                                        <div class="main-content">
                                            <h4 class="name">
                                                {{ $testimonio->vchnombre }} {{ $testimonio->vchapellidos }}
                                            </h4>
                                            <p>
                                                {{ $testimonio->vchtestimonio }}
                                            </p>
                                            {{--
                                            <p>
                                                #SoyVoluntario
                                            </p>
                                            --}}
                                        </div>
                                    </img>
                                </div>
                                @endforeach
                                <!--/ End Single Testimonial -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ End Testimonials -->
            <!-- Faqs -->
            <section class="faq page section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title">
                                <h2>
                                    <span>
                                        Galería
                                    </span>
                                </h2>
                                <p>
                                    fotos
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ End Faqs -->
        </div>
        <div id="registro">
            <!-- Contact Us -->
            <section class="contact section" id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h2>
                                    <span>
                                        Registro
                                    </span>
                                    para ser voluntario
                                </h2>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="contact-head">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <div class="form-head">
                                    {{-- --}}
                                    <!-- Form -->
                                    <form autocomplete="off" class="form" enctype="multipart/form-data" id="frmvoluntario" method="post" name="frmvoluntario" role="form">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">
                                                        Nombre
                                                    </label>
                                                    <input class="form-control" id="nombre" name="nombre" placeholder="Ingrese su nombre" type="text">
                                                        <div id="request_nombre">
                                                        </div>
                                                    </input>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">
                                                        Apellidos
                                                    </label>
                                                    <input class="form-control" id="apellidos" name="apellidos" placeholder="Ingrese sus apellidos" type="text">
                                                        <div id="request_apellidos">
                                                        </div>
                                                    </input>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="">
                                                            Dirección
                                                        </label>
                                                        <textarea class="form-control" id="direccion" name="direccion" placeholder="Ingrese su dirección" rows="1" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;">
                                                        </textarea>
                                                        <div id="request_direccion">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">
                                                                Teléfono
                                                            </label>
                                                            <input class="form-control" id="telefono" name="telefono" placeholder="Ingrese su núm. de teléfono" type="text">
                                                                <div id="request_telefono">
                                                                </div>
                                                            </input>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">
                                                                Correo electrónico
                                                            </label>
                                                            <input class="form-control" id="email" name="email" placeholder="Ingrese su correo electrónico" type="text">
                                                                <div id="request_email">
                                                                </div>
                                                            </input>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">
                                                                    Género
                                                                </label>
                                                                <select class="form-control" id="genero" name="genero">
                                                                    <option value="Masculino">
                                                                        Masculino
                                                                    </option>
                                                                    <option value="Femenino">
                                                                        Femenino
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="">
                                                                    Ocupación
                                                                </label>
                                                                <select class="form-control" id="ocupacion" name="ocupacion">
                                                                    @foreach ($ocupacion as $ocupacion)
                                                                    <option value="{{ $ocupacion->vchocupacion }}">
                                                                        {{ $ocupacion->vchocupacion }}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="">
                                                                        Fecha de nacimiento
                                                                    </label>
                                                                    <?php
                                                                            //fecha actual
                                                                            $hoy = date("Y-m-d");
                                                                            //parte la fecha actual
                                                                            $array = explode("-", $hoy);
                                                                            $dia_actual = $array[2];
                                                                            $mes_actual = $array[1];
                                                                            $ano_actual = $array[0];
                                                                            //fecha maximo = año actual menos 18 años
                                                                            $maximo=$ano_actual-18 . "-" . $mes_actual . "-" . $dia_actual;
                                                                            //fecha minimo = fecha actual menos 58 años
                                                                            $minimo=$ano_actual-68 . "-" . $mes_actual . "-" . $dia_actual;
                                                                        ?>
                                                                    <input class="form-control" id="fecha_de_nacimiento" max="{{ $maximo }}" min="{{ $minimo }}" name="fecha_de_nacimiento" placeholder="Seleccione su fecha de nacimiento" type="date">
                                                                    </input>
                                                                </div>
                                                                <div id="request_fecha_de_nacimiento">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="">
                                                                        Disponibilidad
                                                                    </label>
                                                                    <select class="form-control" id="disponibilidad" name="disponibilidad">
                                                                        @foreach($disponibilidad as $disponibilidad)
                                                                        <option value="{{ $disponibilidad->vchdisponibilidad }}">
                                                                            {{ $disponibilidad->vchdisponibilidad }}
                                                                        </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="">
                                                                            ¿Por qué quieres ser voluntario?
                                                                        </label>
                                                                        <textarea class="form-control" id="justificacion" name="justificacion" placeholder="Ingrese el porque quieres ser voluntario" rows="1" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;">
                                                                        </textarea>
                                                                        <div id="request_justificacion">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="">
                                                                                Foto reciente
                                                                            </label>
                                                                            <input class="" id="foto_perfil" name="foto_perfil" placeholder="Seleccione una foto reciente" type="file">
                                                                                <div id="request_foto">
                                                                                </div>
                                                                            </input>
                                                                        </div>
                                                                        <input class="form-control" id="fecha_registro" name="fecha_registro" type="hidden">
                                                                            {{--
                                                                            <input class="form-control" id="estado" name="estado" type="hidden" value="Inactivo">
                                                                                --}}
                                                                                <input class="form-control" disabled="true" id="token" name="token" type="hidden" value="{{ csrf_token() }}"/>
                                                                                <div id="request_foto_perfil">
                                                                                </div>
                                                                            </input>
                                                                        </input>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                    <div class="form-group">
                                                                        <div class="button">
                                                                            <button class="btn primary" id="btnGuardar" name="btnGuardar" type="submit">
                                                                                Guardar
                                                                            </button>
                                                                            {{-- type="button" --}}
                                                                        </div>
                                                                    </div>
                                                                    <span id="form_output">
                                                                    </span>
                                                                </br>
                                                            </br>
                                                        </br>
                                                    </br>
                                                </br>
                                            </br>
                                        </br>
                                    </form>
                                    <!--/ End Form -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ End Contact Us -->
        </div>
        <!-- Footer -->
        <footer class="footer overlay section wow fadeIn">
            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="bottom-head">
                                <div class="row">
                                    <div class="col-12">
                                        <!-- Social -->
                                        <ul class="social">
                                            <li>
                                                <a href="https://twitter.com/?lang=es" target="_blank">
                                                    <i class="fa fa-twitter">
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://es-la.facebook.com/" target="_blank">
                                                    <i class="fa fa-facebook">
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.youtube.com/?hl=es-419&gl=MX" target="_blank">
                                                    <i class="fa fa-youtube">
                                                    </i>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- End Social -->
                                        <!-- Copyright -->
                                        <div class="copyright">
                                            <p>
                                                © Copyright 2018. Programa Integral de Inclusión
                                                <a href="#">
                                                    EHECATL
                                                </a>
                                                . Todos los derechos reservados.
                                            </p>
                                        </div>
                                        <!--/ End Copyright -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ End Footer Bottom -->
        </footer>
        <!--/ End Footer -->
    </div>
    <script src="assets/js/jquery.min.js">
    </script>
    <script src="assets/js/jquery-migrate.min.js">
    </script>
    <!-- Popper JS-->
    <script src="assets/js/popper.min.js">
    </script>
    <!-- Bootstrap JS-->
    <script src="assets/js/bootstrap.min.js">
    </script>
    <!-- Colors JS-->
    <script src="assets/js/colors.js">
    </script>
    <!-- Jquery Steller JS -->
    <script src="assets/js/jquery.stellar.min.js">
    </script>
    <!-- Particle JS -->
    <script src="assets/js/particles.min.js">
    </script>
    <!-- Fancy Box JS-->
    <script src="assets/js/facnybox.min.js">
    </script>
    <!-- Magnific Popup JS-->
    <script src="assets/js/jquery.magnific-popup.min.js">
    </script>
    <!-- Masonry JS-->
    <script src="assets/js/masonry.pkgd.min.js">
    </script>
    <!-- Circle Progress JS -->
    <script src="assets/js/circle-progress.min.js">
    </script>
    <!-- Owl Carousel JS-->
    <script src="assets/js/owl.carousel.min.js">
    </script>
    <!-- Waypoints JS-->
    <script src="assets/js/waypoints.min.js">
    </script>
    <!-- Slick Nav JS-->
    <script src="assets/js/slicknav.min.js">
    </script>
    <!-- Counter Up JS -->
    <script src="assets/js/jquery.counterup.min.js">
    </script>
    <!-- Easing JS-->
    <script src="assets/js/easing.min.js">
    </script>
    <!-- Wow Min JS-->
    <script src="assets/js/wow.min.js">
    </script>
    <!-- Scroll Up JS-->
    <script src="assets/js/jquery.scrollUp.min.js">
    </script>
    <!-- Google Maps JS -->
    <!-- <script src="assets/js/gmaps.min.js"></script> -->
    <!-- Main JS-->
    <script src="assets/js/main.js">
    </script>
    <!-- Voluntariado -->
    <script src="assets/voluntariado.js">
    </script>
    <!-- sweetalert -->
    <script src="js/sweetalert.min.js">
    </script>
</body>
