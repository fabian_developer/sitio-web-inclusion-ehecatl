<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <!-- Favicon -->
        <link href="../assets2/images/logo/favicon.png" rel="shortcut icon"/>
        <!-- core dependcies css -->
        <link href="../assets2/vendor/bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>
        <link href="../assets2/vendor/PACE/themes/blue/pace-theme-minimal.css" rel="stylesheet"/>
        <link href="../assets2/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <!-- page css -->
        <link href="../assets2/vendor/jvectormap-master/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
        <!-- core css -->
        <link href="../assets2/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="../assets2/css/themify-icons.css" rel="stylesheet"/>
        <link href="../assets2/css/materialdesignicons.min.css" rel="stylesheet"/>
        <link href="../assets2/css/animate.min.css" rel="stylesheet"/>
        <link href="../assets2/css/app.css" rel="stylesheet"/>
        <!--contenedor -->
        <link href="../assets2/css/main.css" rel="stylesheet"/>
    </head>
</html>
<body>
    <div id="app">
        <!-- Book Preloader -->
        <div class="book_preload" id="contenedor">
            <div class="book">
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
                <div class="book__page">
                </div>
            </div>
        </div>
        <!--/ End Book Preloader -->
        <div class="app header-primary side-nav-dark">
            {{-- temas: info - warning   header-info-gradient  --}}
            <div class="layout">
                <!-- Header START -->
                <div class="header navbar">
                    <div class="header-container">
                        <div class="nav-logo">
                            <a href="index.html">
                                <div class="logo logo-dark" style="background-image: url('../assets2/images/logo/logo.png')">
                                </div>
                                <div class="logo logo-white" style="background-image: url('../assets2/images/logo/logo-white.png')">
                                </div>
                            </a>
                        </div>
                        <ul class="nav-left">
                            <li>
                                <a class="sidenav-fold-toggler" href="javascript:void(0);">
                                    <i class="mdi mdi-menu">
                                    </i>
                                </a>
                                <a class="sidenav-expand-toggler" href="javascript:void(0);">
                                    <i class="mdi mdi-menu">
                                    </i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="notifications dropdown dropdown-animated scale-left">
                                <span class="counter">
                                    2
                                </span>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="mdi mdi-email-outline">
                                    </i>
                                </a>
                            </li>
                            <li class="user-profile dropdown dropdown-animated scale-left">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <div class="media-img">
                                        <img alt="" class="img-fluid img-circle" src="../../voluntario/foto/{{ $foto }}" style="width: 35px; height: 35px">
                                            {{-- profile-img --}}
                                            <strong>
                                                {{--  {{ $nombre }} {{ $apellidos }} --}}
                                            </strong>
                                        </img>
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-md p-v-0">
                                    <li>
                                        <ul class="list-media">
                                            <li class="list-item p-15">
                                                <div class="media-img">
                                                    <img alt="" src="../../voluntario/foto/{{ $foto }}">
                                                    </img>
                                                </div>
                                                <div class="info">
                                                    <span class="title text-semibold">
                                                        {{ $nombre }} {{ $apellidos }}
                                                    </span>
                                                    <span class="sub-title">
                                                        {{ $email }}
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="divider" role="separator">
                                    </li>
                                    <li>
                                        <a href="perfil">
                                            <i class="ti-user p-r-10">
                                            </i>
                                            <span>
                                                Perfil
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="ti-power-off p-r-10">
                                            </i>
                                            <span>
                                                Cerrar sesión
                                            </span>
                                        </a>
                                        <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Header END -->
                <!-- Side Nav START menu izquierda-->
                <div class="side-nav expand-lg">
                    <div class="side-nav-inner">
                        <ul class="side-nav-menu scrollable">
                            <li class="side-nav-header">
                                <span>
                                    Modulos
                                </span>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="index">
                                    {{-- javascript:void(0); --}}
                                    <span class="icon-holder">
                                        <i class="mdi mdi-desktop-mac">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Principal
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="voluntarios">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-account-multiple-outline">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Voluntarios
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown open active">
                                <a class="dropdown-toggle" href="actividades">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-presentation-play">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Actividades
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="multimedia">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-image-filter">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Multimedia
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="padres">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-human-male-female">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Padres
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="ninios">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-walk">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Niños
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="usuarios">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-account">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Usuarios
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="noticias">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-book-multiple-variant">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Noticias
                                    </span>
                                </a>
                            </li>
                            <li class="side-nav-header">
                                <span>
                                    Configuración
                                </span>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="ocupacion">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-account-switch">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Ocupacion
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="disponibilidad">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-timetable">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Disponibilidad
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="transporte">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-car">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Transporte
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="lugares">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-panorama">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Lugares
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Side Nav END -->
                <!-- Page Container START -->
                <div class="page-container">
                    <!-- Content Wrapper START -->
                    <div class="main-content">
                        <div class="container-fluid">
                            <div class="page-header">
                                <h2 class="header-title">
                                    Actividades
                                </h2>
                                {{--
                                <div class="header-sub-title">
                                    <button class="btn btn-primary" id="btnNuevoRegistroActividad" type="button">
                                        Nuevo registro
                                    </button>
                                </div>
                                --}}
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-overflow">
                                        <table cellspacing="0" class="table table-striped nowrap table-hover table-bordered cell-border order-column" id="tabla_actividades" name="tabla_actividades" width="100%">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>
                                                        Clave
                                                    </th>
                                                    <th>
                                                        Nombre
                                                    </th>
                                                    <th>
                                                        Descripción
                                                    </th>
                                                    <th>
                                                        Fecha
                                                    </th>
                                                    <th>
                                                        Hora inicio
                                                    </th>
                                                    <th>
                                                        Hora termino
                                                    </th>
                                                    <th>
                                                        Voluntarios
                                                    </th>
                                                    <th>
                                                        Niños
                                                    </th>
                                                    <th>
                                                        Clave lugar
                                                    </th>
                                                    <th>
                                                        Lugar
                                                    </th>
                                                    <th>
                                                        Clave transporte
                                                    </th>
                                                    <th>
                                                        Transporte
                                                    </th>
                                                    <th>
                                                        Estatus
                                                    </th>
                                                    <th>
                                                        Opciones
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="page-header">
                                <h2 class="header-title">
                                    Voluntarios
                                </h2>
                                {{--
                                <div class="header-sub-title">
                                    <button class="btn btn-primary" id="btnNuevoRegistroActividad" type="button">
                                        Nuevo registro
                                    </button>
                                </div>
                                --}}
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-overflow">
                                        <table cellspacing="0" class="table table-striped nowrap table-hover table-bordered cell-border order-column" id="tabla_voluntarios" name="tabla_voluntarios" width="100%">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>
                                                        Clave
                                                    </th>
                                                    <th>
                                                        Actividad
                                                    </th>
                                                    <th>
                                                        Voluntarios
                                                    </th>
                                                    {{--
                                                    <th>
                                                        Estatus
                                                    </th>
                                                    --}}
                                                    <th>
                                                        Opciones
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Wrapper END -->
                    <!-- Footer START -->
                    <footer class="content-footer">
                        <div class="footer">
                            <div class="copyright">
                                <span>
                                    © Copyright 2018.
                                    <b class="text-dark">
                                        Programa Integral de Inclusión EHECATL.
                                    </b>
                                    Todos los derechos reservados.
                                </span>
                            </div>
                        </div>
                    </footer>
                    <!-- Footer END -->
                </div>
                <!-- Page Container END -->
            </div>
        </div>
        {{-- @yield('footer') --}}
    </div>
    {{-- Modal guardar y actualizar registro --}}
    <div class="modal fade" id="frm-modal-actividades">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#223143;">
                    <h5 class="modal-title" id="titulo_modal" style="color:white">
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" style="color:white" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="p-15 m-v-40">
                        <form autocomplete="off" method="post">
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">
                                            Actividad
                                        </label>
                                        <input class="form-control" id="clave" name="clave" type="hidden">
                                            <input class="form-control" id="actividad" name="actividad" placeholder="Ingrese el nombre de la actividad" type="text">
                                                <div id="request_actividad">
                                                </div>
                                            </input>
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">
                                            Descripción
                                        </label>
                                        <textarea class="form-control" id="descripcion" name="descripcion" rows="1" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;">
                                        </textarea>
                                        <div id="request_descripcion">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">
                                            Fecha
                                        </label>
                                        <?php 
                                            //fecha actual
                                            $hoy = date("Y-m-d");
                                            //parte la fecha actual
                                            $array = explode("-", $hoy);
                                            $dia_actual = $array[2];
                                            $mes_actual = $array[1];
                                            $ano_actual = $array[0];
                                            //fecha maximo = año actual menos 18 años 
                                            $minimo=$ano_actual . "-" . $mes_actual . "-" . $dia_actual;
                                            //fecha minimo = fecha actual menos 58 años
                                            //$minimo=$ano_actual-68 . "-" . $mes_actual . "-" . $dia_actual;
                                        ?>
                                        <input class="form-control" id="fecha" min="{{ $minimo }}" name="fecha" type="date">
                                            <div id="request_fecha">
                                            </div>
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">
                                            Hora inicio
                                        </label>
                                        <input class="form-control" id="hora_inicio" name="hora_inicio" placeholder="Ingrese el lugar" type="time">
                                            <div id="request_hora_inicio">
                                            </div>
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">
                                            Hora termino
                                        </label>
                                        <input class="form-control" id="hora_termino" name="hora_termino" placeholder="Ingrese el lugar" type="time">
                                            <div id="request_hora_termino">
                                            </div>
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Núm. voluntarios
                                        </label>
                                        <input class="form-control" id="voluntarios" name="voluntarios" placeholder="Ingrese el lugar" type="number">
                                            <div id="request_voluntarios">
                                            </div>
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Núm. niños
                                        </label>
                                        <input class="form-control" id="ninios" name="ninios" placeholder="Ingrese el lugar" type="number">
                                            <div id="request_ninios">
                                            </div>
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Lugar
                                        </label>
                                        <input class="form-control" id="lugar" name="lugar" placeholder="" type="text">
                                        </input>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">
                                            Medio de transporte
                                        </label>
                                        <input class="form-control" id="transporte" name="transporte" placeholder="" type="text">
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control" id="token" name="token" readonly="" type="text" value="{{ csrf_token() }}"/>
                                </div>
                            </div>
                            <span id="form_output">
                            </span>
                            <div class="modal-footer">
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-block btn-default" id="btnCerrar" name="btnCerrar" type="button" value="Cerrar">
                                    </input>
                                </div>
                                <div class="col-md-4">
                                    <input class="btn btn-block btn-info" id="btnGuardar" name="btnGuardar" type="button" value="Guardar">
                                    </input>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- jquery --}}
    <script src="../assets2/js/jquery.min.js">
    </script>
    <!-- actividad -->
    <script src="../assets2/js-adm/actividades.js">
    </script>
    <!-- fin - voluntario -->
    <script src="../assets2/js/vendor.js">
    </script>
    <script src="../assets2/js/app.min.js">
    </script>
    <!-- page js -->
    <script src="../assets2/vendor/chart.js/dist/Chart.min.js">
    </script>
    <script src="../assets2/vendor/datatables/media/js/jquery.dataTables.js">
    </script>
    <script src="../assets2/vendor/datatables/media/js/dataTables.bootstrap4.min.js">
    </script>
    <script src="../assets2/js/tables/data-table.js">
    </script>
    <script src="../js/sweetalert.min.js">
    </script>
</body>