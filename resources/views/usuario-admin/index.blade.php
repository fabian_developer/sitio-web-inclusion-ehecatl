<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        <title>
            Programa Integral de Inclusión EHECATL
        </title>
        <!-- Favicon -->
        <link href="../assets2/images/logo/apple-touch-icon.html" rel="apple-touch-icon"/>
        <link href="../assets2/images/logo/favicon.png" rel="shortcut icon"/>
        <!-- core dependcies css -->
        <link href="../assets2/vendor/bootstrap/dist/css/bootstrap.css" rel="stylesheet"/>
        <link href="../assets2/vendor/PACE/themes/blue/pace-theme-minimal.css" rel="stylesheet"/>
        <link href="../assets2/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <!-- page css -->
        <link href="../assets2/vendor/jvectormap-master/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
        <!-- core css -->
        <link href="../assets2/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="../assets2/css/themify-icons.css" rel="stylesheet"/>
        <link href="../assets2/css/materialdesignicons.min.css" rel="stylesheet"/>
        <link href="../assets2/css/animate.min.css" rel="stylesheet"/>
        <link href="../assets2/css/app.css" rel="stylesheet"/>
    </head>
</html>
<body>
    <div id="app">
        <div class="app header-primary side-nav-dark">
            {{-- temas: info - warning --}}
            <div class="layout">
                <!-- Header START -->
                <div class="header navbar">
                    <div class="header-container">
                        <div class="nav-logo">
                            <a href="index.html">
                                <div class="logo logo-dark" style="background-image: url('../assets2/images/logo/logo.png')">
                                </div>
                                <div class="logo logo-white" style="background-image: url('../assets2/images/logo/logo-white.png')">
                                </div>
                            </a>
                        </div>
                        <ul class="nav-left">
                            <li>
                                <a class="sidenav-fold-toggler" href="javascript:void(0);">
                                    <i class="mdi mdi-menu">
                                    </i>
                                </a>
                                <a class="sidenav-expand-toggler" href="javascript:void(0);">
                                    <i class="mdi mdi-menu">
                                    </i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="notifications dropdown dropdown-animated scale-left">
                                <span class="counter">
                                    2
                                </span>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="mdi mdi-email-outline">
                                    </i>
                                </a>
                            </li>
                            <li class="user-profile dropdown dropdown-animated scale-left">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <div class="media-img">
                                        <img alt="" class="img-fluid img-circle" src="../../voluntario/foto/{{ $foto }}" style="width: 35px; height: 35px">
                                            {{-- profile-img --}}
                                            <strong>
                                                {{--  {{ $nombre }} {{ $apellidos }} --}}
                                            </strong>
                                        </img>
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-md p-v-0">
                                    <li>
                                        <ul class="list-media">
                                            <li class="list-item p-15">
                                                <div class="media-img">
                                                    <img alt="" src="../../voluntario/foto/{{ $foto }}">
                                                    </img>
                                                </div>
                                                <div class="info">
                                                    <span class="title text-semibold">
                                                        {{ $nombre }} {{ $apellidos }}
                                                    </span>
                                                    <span class="sub-title">
                                                        {{ $email }}
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="divider" role="separator">
                                    </li>
                                    <li>
                                        <a href="perfil">
                                            <i class="ti-user p-r-10">
                                            </i>
                                            <span>
                                                Perfil
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="ti-power-off p-r-10">
                                            </i>
                                            <span>
                                                Cerrar sesión
                                            </span>
                                        </a>
                                        <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Header END -->
                <!-- Side Nav START menu izquierda-->
                <div class="side-nav expand-lg">
                    <div class="side-nav-inner">
                        <ul class="side-nav-menu scrollable">
                            <li class="side-nav-header">
                                <span>
                                    Modulos
                                </span>
                            </li>
                            <li class="nav-item dropdown open active">
                                <a class="dropdown-toggle" href="index">
                                    {{-- javascript:void(0); --}}
                                    <span class="icon-holder">
                                        <i class="mdi mdi-desktop-mac">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Principal
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="voluntarios">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-account-multiple-outline">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Voluntarios
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="actividades">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-presentation-play">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Actividades
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="multimedia">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-image-filter">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Multimedia
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="padres">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-human-male-female">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Padres
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="ninios">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-walk">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Niños
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="usuarios">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-account">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Usuarios
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="noticias">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-book-multiple-variant">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Noticias
                                    </span>
                                </a>
                            </li>
                            <li class="side-nav-header">
                                <span>
                                    Configuración
                                </span>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="ocupacion">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-account-switch">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Ocupacion
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="disponibilidad">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-timetable">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Disponibilidad
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="transporte">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-car">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Transporte
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="dropdown-toggle" href="lugares">
                                    <span class="icon-holder">
                                        <i class="mdi mdi-panorama">
                                        </i>
                                    </span>
                                    <span class="title">
                                        Lugares
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Side Nav END -->
                <!-- Page Container START -->
                <div class="page-container">
                    <!-- Content Wrapper START -->
                    <div class="main-content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body ">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-user font-size-40 icon-gradient-success text-success">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Voluntarios activos
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0">
                                                        5
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-palette font-size-40 icon-gradient-info text-info">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Actividades
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0">
                                                        38
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-bar-chart font-size-40 icon-gradient-primary text-primary">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Growth
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0 text-success">
                                                        <span>
                                                            18%
                                                        </span>
                                                        <i class="ti-arrow-up font-size-14">
                                                        </i>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media">
                                                <div class="align-self-center">
                                                    <i class="ti-reload font-size-40 icon-gradient-danger text-danger">
                                                    </i>
                                                </div>
                                                <div class="m-l-20">
                                                    <p class="m-b-0">
                                                        Transaction
                                                    </p>
                                                    <h2 class="font-weight-light m-b-0">
                                                        <span>
                                                            $21,338
                                                        </span>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Wrapper END -->
                    <!-- Footer START -->
                    <footer class="content-footer">
                        <div class="footer">
                            <div class="copyright">
                                <span>
                                    Copyright © 2018
                                    <b class="text-dark">
                                        Programa integral de Inclusión
                                    </b>
                                    . Todos los derechos reservados.
                                </span>
                                {{--
                                <span class="go-right">
                                    <a class="text-gray m-r-15" href="#">
                                        Term & Conditions
                                    </a>
                                    <a class="text-gray" href="#">
                                        Privacy & Policy
                                    </a>
                                </span>
                                --}}
                            </div>
                        </div>
                    </footer>
                    <!-- Footer END -->
                </div>
                <!-- Page Container END -->
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js">
    </script>
    <script src="../assets2/js/vendor.js">
    </script>
    <script src="../assets2/js/app.min.js">
    </script>
    <!-- page js -->
    <script src="../assets2/vendor/chart.js/dist/Chart.min.js">
    </script>
    <script src="../assets2/vendor/datatables/media/js/jquery.dataTables.js">
    </script>
    <script src="../assets2/vendor/datatables/media/js/dataTables.bootstrap4.min.js">
    </script>
    <script src="../assets2/js/tables/data-table.js">
    </script>
    <!-- Voluntario -->
    {{--
    <script src="../js/voluntario.js">
    </script>
    --}}
    <!-- sweetalert -->
    {{--
    <script src="assets/js/sweetalert.min.js">
    </script>
    --}}
</body>