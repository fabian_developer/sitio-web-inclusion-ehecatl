<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>Programa Integral de Inclusión EHECATL</title>
    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
      <!-- Favicon -->
    <link rel="apple-touch-icon" href="assets2/images/logo/apple-touch-icon.html">
    <link rel="shortcut icon" href="assets2/images/logo/favicon.png">

    <!-- core dependcies css -->
    <link rel="stylesheet" href="assets2/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="assets2/vendor/PACE/themes/blue/pace-theme-minimal.css" />
    <link rel="stylesheet" href="assets2/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" />

    <!-- page css -->

    <!-- core css -->
    <link href="assets2/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets2/css/themify-icons.css" rel="stylesheet">
    <link href="assets2/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="assets2/css/animate.min.css" rel="stylesheet">
    <link href="assets2/css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="assets2/js/vendor.js"></script>
    <script src="assets2/js/app.min.js"></script>

</body>
</html>
