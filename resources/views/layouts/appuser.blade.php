
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
       
        <!-- CSRF Token -->
        <meta content="{{ csrf_token() }}" name="csrf-token"/>
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Voluntario</title>

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="../assets2/images/logo/apple-touch-icon.html">
        <link rel="shortcut icon" href="../assets2/images/logo/favicon.png">

        <!-- core dependcies css -->
        <link rel="stylesheet" href="../assets2/vendor/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="../assets2/vendor/PACE/themes/blue/pace-theme-minimal.css" />
        <link rel="stylesheet" href="../assets2/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css" />

        <!-- page css -->
        <link rel="stylesheet" href="../assets2/vendor/jvectormap-master/jquery-jvectormap-2.0.3.css" />

        <!-- core css -->
        <link href="../assets2/css/font-awesome.min.css" rel="stylesheet">
        <link href="../assets2/css/themify-icons.css" rel="stylesheet">
        <link href="../assets2/css/materialdesignicons.min.css" rel="stylesheet">
        <link href="../assets2/css/animate.min.css" rel="stylesheet">
        <link href="../assets2/css/app.css" rel="stylesheet">
    </head>
</html>
<body>
    <div id="app">
        @yield('content')
        @yield('footer')
    </div>
    <!-- Scripts -->
    <script src="../assets2/js/vendor.js"></script>
    <script src="../assets2/js/app.min.js"></script>

    <!-- page js -->
    <script src="../assets2/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="../assets2/vendor/jvectormap-master/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="../assets2/js/maps/vector-map-lib/jquery-jvectormap-world-mill.js"></script>
    <script src="../assets2/js/dashboard/saas.js"></script>
</body>
