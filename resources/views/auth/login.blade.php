@extends('layouts.applogin') 
@section('content')
<div class="app">
        <div class="layout" style="background-color: white;">
            <div class="container" >
                <div class="row full-height align-items-center">
                    <div class="col-md-5 mx-auto">
                        <div class="card">
                            <div class="card-body">
                                <div class="p-15">
                                    <div class="m-b-30 text-center">
                                        <img src="assets/images/logo_login.png" alt="">
                                        <br>
                                        <h2>Iniciar sesión</h2>
                                    </div>
                                    <p class="m-t-15 font-size-13">Por favor, ingrese su correo electrónico  y contraseña para iniciar sesión</p>
                                    <form autocomplete="off" method="POST" action="{{ route('login') }}">
                                         {{ csrf_field() }}
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input id="email" type="email" class="form-control"  placeholder="Correo electrónico" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input id="password" type="password" class="form-control" placeholder="Contraseña" name="password" required> @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="checkbox font-size-13 d-inline-block p-v-0 m-v-0">
                                            
                                        </div>
                                        <div class="pull-right">
                                            <a class="" href="{{ route('password.request') }}">
                                                ¿Olvidaste tu contraseña?
                                            </a>
                                        </div>
                                        <div class="m-t-20 text-right">
                                            <button type="submit" class="btn btn-primary">
                                                Iniciar sesión
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection