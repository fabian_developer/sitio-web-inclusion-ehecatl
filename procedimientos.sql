*************************************************************************************************
****                      Procedimiento para la vista de actividades                          ***
*************************************************************************************************

DELIMITER $$

USE `voluntariado`$$

DROP PROCEDURE IF EXISTS `proc_vista_actividades`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_vista_actividades`()
    NO SQL
SELECT 
		tblactividad.intidactividad AS clave_actividad,
    	tblactividad.vchnombre AS actividad,
    	tblactividad.vchdescripcion AS descripcion,
    	tblactividad.vchfecha AS fecha,
    	tblactividad.vchhora_inicio AS hora_inicio, 
    	tblactividad.vchhora_termino AS hora_termino,
    	tblactividad.intnum_voluntarios AS num_voluntarios,
    	tblactividad.intnum_ninios AS num_ninios,
    	tblactividad.intidlugar AS clave_lugar,
    	tbllugar.vchlugar AS lugar,
    	tblactividad.intid_tipo_transporte AS clave_tipo_transporte,
    	tbltipo_transporte.vchtipo_transporte AS tipo_transporte,
    	tblactividad.vchestado AS estado
	FROM 
		tblactividad,
    	tbllugar,
    	tbltipo_transporte
	WHERE 
		tblactividad.intidlugar=tbllugar.intidlugar AND
        tblactividad.intid_tipo_transporte=tbltipo_transporte.intid_tipo_transporte$$

DELIMITER ;

*************************************************************************************************


