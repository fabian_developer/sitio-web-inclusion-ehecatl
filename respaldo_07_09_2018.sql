

/*Table structure for table `tblactividad` */

DROP TABLE IF EXISTS `tblactividad`;

CREATE TABLE `tblactividad` (
  `intidactividad` int(10) NOT NULL AUTO_INCREMENT,
  `vchnombre` varchar(250) NOT NULL,
  `vchdescripcion` varchar(500) NOT NULL,
  `vchfecha` varchar(50) NOT NULL,
  `vchhora_inicio` varchar(50) NOT NULL,
  `vchhora_termino` varchar(50) NOT NULL,
  `intnum_voluntarios` int(5) NOT NULL,
  `intnum_ninios` int(5) NOT NULL,
  `intidlugar` int(10) NOT NULL,
  `intid_tipo_transporte` int(10) NOT NULL,
  `vchestado` varchar(50) NOT NULL,
  PRIMARY KEY (`intidactividad`),
  KEY `Relacion_tbltransporte` (`intid_tipo_transporte`),
  KEY `Relacion_tbllugar` (`intidlugar`),
  CONSTRAINT `Relacion_tbllugar` FOREIGN KEY (`intidlugar`) REFERENCES `tbllugar` (`intidlugar`),
  CONSTRAINT `Relacion_tbltransporte` FOREIGN KEY (`intid_tipo_transporte`) REFERENCES `tbltipo_transporte` (`intid_tipo_transporte`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `tblactividad` */

insert  into `tblactividad`(`intidactividad`,`vchnombre`,`vchdescripcion`,`vchfecha`,`vchhora_inicio`,`vchhora_termino`,`intnum_voluntarios`,`intnum_ninios`,`intidlugar`,`intid_tipo_transporte`,`vchestado`) values (2,'paseo al parque de tehuetlan','paseo al parque de tehuetlan','2018-09-20','02:00','01:00',2,4,1,1,'En espera'),(4,'prueba prueba prueba prueba','prueba prueba prueba prueba','2018-09-19','00:42','23:41',2,4,1,1,'En espera'),(5,'fvrvr gr rtg','kjkjlkjtr grt g rtg tr g','2019-10-07','02:45','23:45',1,1,1,1,'En espera'),(6,'jghhjghfhf hjfhjfjh',',knm,nm,nmnm hbmhhgjh','2018-09-01','01:47','23:47',1,1,1,1,'En espera'),(9,'lkfklvdkjfvnjkdnvjkdfnvjdf','jnjknjnjnjnkjnjknkjjn','2019-10-08','09:27','09:27',1,1,1,1,'En espera'),(10,'kjhrgrhgiurg huhuuuhhu','hukjgirhuierer','2018-09-26','13:05','10:05',2,1,1,1,'En espera');

/*Table structure for table `tblactividad_voluntariado` */

DROP TABLE IF EXISTS `tblactividad_voluntariado`;

CREATE TABLE `tblactividad_voluntariado` (
  `intid_activ_volun` int(10) NOT NULL AUTO_INCREMENT,
  `vchfecha` varchar(50) NOT NULL,
  `vchhora_inicio` varchar(50) NOT NULL,
  `vchhora_termino` varchar(50) NOT NULL,
  `intidvoluntariado` int(10) NOT NULL,
  `intidactividad` int(10) NOT NULL,
  PRIMARY KEY (`intid_activ_volun`),
  KEY `Relacion_tblactividad_` (`intidactividad`),
  KEY `Relacion_tblvoluntariado` (`intidvoluntariado`),
  CONSTRAINT `Relacion_tblactividad_` FOREIGN KEY (`intidactividad`) REFERENCES `tblactividad` (`intidactividad`),
  CONSTRAINT `Relacion_tblvoluntariado` FOREIGN KEY (`intidvoluntariado`) REFERENCES `tblvoluntariado` (`intidvoluntariado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblactividad_voluntariado` */

/*Table structure for table `tbldiscapacidad` */

DROP TABLE IF EXISTS `tbldiscapacidad`;

CREATE TABLE `tbldiscapacidad` (
  `intIdDiscapacidad` int(11) NOT NULL AUTO_INCREMENT,
  `vchNombre` varchar(30) NOT NULL,
  `vchDescripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`intIdDiscapacidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbldiscapacidad` */

/*Table structure for table `tbldisponibilidad` */

DROP TABLE IF EXISTS `tbldisponibilidad`;

CREATE TABLE `tbldisponibilidad` (
  `intiddisponibilidad` int(10) NOT NULL AUTO_INCREMENT,
  `vchdisponibilidad` varchar(100) NOT NULL,
  PRIMARY KEY (`intiddisponibilidad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tbldisponibilidad` */

insert  into `tbldisponibilidad`(`intiddisponibilidad`,`vchdisponibilidad`) values (1,'Mañana'),(2,'Tarde'),(3,'Fines de semana'),(4,'Indiferente');

/*Table structure for table `tblhistorial` */

DROP TABLE IF EXISTS `tblhistorial`;

CREATE TABLE `tblhistorial` (
  `inIdNinio` int(11) DEFAULT NULL,
  `vchAntHereditario` varchar(300) DEFAULT NULL,
  `vchEnferAnterior` varchar(300) DEFAULT NULL,
  `vchOperaciones` varchar(300) DEFAULT NULL,
  `vchAlergias` varchar(300) DEFAULT NULL,
  `vchEnferActual` varchar(300) DEFAULT NULL,
  `vchObservaciones` varchar(300) DEFAULT NULL,
  `vchComprobacion` varchar(10) DEFAULT NULL,
  KEY `inIdNinio` (`inIdNinio`),
  KEY `inIdNinio_2` (`inIdNinio`),
  CONSTRAINT `tblhistorial_ibfk_1` FOREIGN KEY (`inIdNinio`) REFERENCES `tblninio` (`inIdNinio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblhistorial` */

/*Table structure for table `tbllugar` */

DROP TABLE IF EXISTS `tbllugar`;

CREATE TABLE `tbllugar` (
  `intidlugar` int(10) NOT NULL AUTO_INCREMENT,
  `vchlugar` varchar(250) NOT NULL,
  PRIMARY KEY (`intidlugar`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbllugar` */

insert  into `tbllugar`(`intidlugar`,`vchlugar`) values (1,'Parque ecologico');

/*Table structure for table `tblmultimedia` */

DROP TABLE IF EXISTS `tblmultimedia`;

CREATE TABLE `tblmultimedia` (
  `intid_multimedia` int(10) NOT NULL AUTO_INCREMENT,
  `vchtitulo` varchar(250) NOT NULL,
  `vchdescripcion` varchar(1000) NOT NULL,
  `vcharchivo` varchar(250) NOT NULL,
  PRIMARY KEY (`intid_multimedia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblmultimedia` */

/*Table structure for table `tblninio` */

DROP TABLE IF EXISTS `tblninio`;

CREATE TABLE `tblninio` (
  `inIdNinio` int(11) NOT NULL AUTO_INCREMENT,
  `vchNombre` varchar(40) NOT NULL,
  `vchApellidoPa` varchar(40) NOT NULL,
  `vchApellidoMa` varchar(40) NOT NULL,
  `vchSexo` varchar(15) NOT NULL,
  `dteFechaNac` date NOT NULL,
  `vchRefDomicilio` varchar(240) NOT NULL,
  `intEdad` int(11) NOT NULL,
  `dbePeso` double DEFAULT NULL,
  `dbeEstatura` double DEFAULT NULL,
  `vchTipoSangre` varchar(15) NOT NULL,
  PRIMARY KEY (`inIdNinio`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tblninio` */

insert  into `tblninio`(`inIdNinio`,`vchNombre`,`vchApellidoPa`,`vchApellidoMa`,`vchSexo`,`dteFechaNac`,`vchRefDomicilio`,`intEdad`,`dbePeso`,`dbeEstatura`,`vchTipoSangre`) values (3,'Alberto','Lara','Hernandez','Masculino','2018-03-07','Col. colalambre',15,60,1.64,'O Negativo'),(5,'Luis','Hernandez','Garcia','Masculino','2018-03-12','Colalambre',18,12,1.68,'Positivo');

/*Table structure for table `tblniniodiscap` */

DROP TABLE IF EXISTS `tblniniodiscap`;

CREATE TABLE `tblniniodiscap` (
  `intIdNinioDisc` int(11) NOT NULL AUTO_INCREMENT,
  `inIdNinio` int(11) DEFAULT NULL,
  `intIdDiscapacidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`intIdNinioDisc`),
  KEY `relacionNinio` (`inIdNinio`),
  KEY `relacionDiscapacidad` (`intIdDiscapacidad`),
  CONSTRAINT `relacionDiscapacidad` FOREIGN KEY (`intIdDiscapacidad`) REFERENCES `tbldiscapacidad` (`intIdDiscapacidad`),
  CONSTRAINT `relacionNinio` FOREIGN KEY (`inIdNinio`) REFERENCES `tblninio` (`inIdNinio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblniniodiscap` */

/*Table structure for table `tblniniopadre` */

DROP TABLE IF EXISTS `tblniniopadre`;

CREATE TABLE `tblniniopadre` (
  `intIdNinioPadre` int(11) NOT NULL AUTO_INCREMENT,
  `intIdPadre` int(11) DEFAULT NULL,
  `inIdNinio` int(11) DEFAULT NULL,
  PRIMARY KEY (`intIdNinioPadre`),
  KEY `relacion_Padre` (`intIdPadre`),
  KEY `relacion_ninio` (`inIdNinio`),
  CONSTRAINT `relacion_Padre` FOREIGN KEY (`intIdPadre`) REFERENCES `tblpadre` (`intIdPadre`),
  CONSTRAINT `relacion_ninio` FOREIGN KEY (`inIdNinio`) REFERENCES `tblninio` (`inIdNinio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tblniniopadre` */

/*Table structure for table `tblocupacion` */

DROP TABLE IF EXISTS `tblocupacion`;

CREATE TABLE `tblocupacion` (
  `intidocupacion` int(10) NOT NULL AUTO_INCREMENT,
  `vchocupacion` varchar(250) NOT NULL,
  PRIMARY KEY (`intidocupacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tblocupacion` */

insert  into `tblocupacion`(`intidocupacion`,`vchocupacion`) values (1,'Estudiante'),(2,'Profesor');

/*Table structure for table `tblpadre` */

DROP TABLE IF EXISTS `tblpadre`;

CREATE TABLE `tblpadre` (
  `intIdPadre` int(11) NOT NULL AUTO_INCREMENT,
  `vchNombre` varchar(40) NOT NULL,
  `vchApellidoPa` varchar(40) NOT NULL,
  `vchApellidoMa` varchar(40) NOT NULL,
  `vchSexo` varchar(12) NOT NULL,
  `dteFechaNac` date NOT NULL,
  `vchDomicilio` varchar(240) NOT NULL,
  `vchReferenciaDom` varchar(240) NOT NULL,
  `vchEscolaridad` varchar(80) NOT NULL,
  `vchOcupacion` varchar(50) NOT NULL,
  `vchTelMovil` varchar(15) NOT NULL,
  `vchTelFijo` varchar(15) NOT NULL,
  `intIdTipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`intIdPadre`),
  KEY `relacion_TipoFamiliar` (`intIdTipo`),
  CONSTRAINT `relacion_TipoFamiliar` FOREIGN KEY (`intIdTipo`) REFERENCES `tbltipofamiliar` (`intIdTipoFam`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tblpadre` */

insert  into `tblpadre`(`intIdPadre`,`vchNombre`,`vchApellidoPa`,`vchApellidoMa`,`vchSexo`,`dteFechaNac`,`vchDomicilio`,`vchReferenciaDom`,`vchEscolaridad`,`vchOcupacion`,`vchTelMovil`,`vchTelFijo`,`intIdTipo`) values (1,'Juan ','vite','Hernandez','maasculino','1975-07-16','zaragoza','porton verde','Secundaria','abogafo','771154504','7711763412',NULL);

/*Table structure for table `tbltipo_transporte` */

DROP TABLE IF EXISTS `tbltipo_transporte`;

CREATE TABLE `tbltipo_transporte` (
  `intid_tipo_transporte` int(10) NOT NULL AUTO_INCREMENT,
  `vchtipo_transporte` varchar(250) NOT NULL,
  PRIMARY KEY (`intid_tipo_transporte`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `tbltipo_transporte` */

insert  into `tbltipo_transporte`(`intid_tipo_transporte`,`vchtipo_transporte`) values (1,'Caminando');

/*Table structure for table `tbltipofamiliar` */

DROP TABLE IF EXISTS `tbltipofamiliar`;

CREATE TABLE `tbltipofamiliar` (
  `intIdTipoFam` int(11) NOT NULL AUTO_INCREMENT,
  `vchTipo` varchar(100) NOT NULL,
  PRIMARY KEY (`intIdTipoFam`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tbltipofamiliar` */

insert  into `tbltipofamiliar`(`intIdTipoFam`,`vchTipo`) values (1,'Padre'),(2,'Madre'),(3,'Tutor');

/*Table structure for table `tblvoluntariado` */

DROP TABLE IF EXISTS `tblvoluntariado`;

CREATE TABLE `tblvoluntariado` (
  `intidvoluntariado` int(10) NOT NULL AUTO_INCREMENT,
  `vchnombre` varchar(250) NOT NULL,
  `vchapellidos` varchar(250) NOT NULL,
  `vchdireccion` varchar(500) NOT NULL,
  `vchtelefono` varchar(15) NOT NULL,
  `vchemail` varchar(250) NOT NULL,
  `vchgenero` varchar(50) NOT NULL,
  `vchocupacion` varchar(250) NOT NULL,
  `vchfecha_nacimiento` varchar(50) NOT NULL,
  `vchdisponibilidad` varchar(50) NOT NULL,
  `vchporque` varchar(500) NOT NULL,
  `vchfoto_perfil` varchar(250) DEFAULT NULL,
  `vchfecha_registro` varchar(100) NOT NULL,
  `vchtestimonio` varchar(1000) DEFAULT NULL,
  `vchestado` varchar(50) NOT NULL,
  PRIMARY KEY (`intidvoluntariado`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Data for the table `tblvoluntariado` */

insert  into `tblvoluntariado`(`intidvoluntariado`,`vchnombre`,`vchapellidos`,`vchdireccion`,`vchtelefono`,`vchemail`,`vchgenero`,`vchocupacion`,`vchfecha_nacimiento`,`vchdisponibilidad`,`vchporque`,`vchfoto_perfil`,`vchfecha_registro`,`vchtestimonio`,`vchestado`) values (27,'Fabian','Santiago Hernández','Loc. Acuatempa Huejutla de Reyes Hidalgo calle 15','7713023710','tecnologias1997@gmail.com','Masculino','Estudiante','2000-09-05','Fines de semana','porque tengo mucho tiempo libre y me isdfhdkhkjdg nfgrgr','1536257469fabian.jpg','Jueves, 6 de Septiembre de 2018 13:9:26','p','Aceptado');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo_usuario` int(5) NOT NULL,
  `confirmado` tinyint(1) NOT NULL,
  `codigo_confirmacion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`tipo_usuario`,`confirmado`,`codigo_confirmacion`) values (53,'example','example@example.com','$2y$10$kVWJ55sAiCI82g64rqbciei6ON9ahfTWHo6jkdOZstikSz6f9BzGm','qTSF8mZy1uqd45RrObCw9uMsU4qNcLfhqtd9ljoT7oWX3UJnamcnc96owtGD',NULL,NULL,0,1,NULL),(54,'Fabian Santiago Hernández','tecnologias1997@gmail.com','$2y$10$GV6GwcxAW5X7r.FeS0sJZOPRukdzHQa.bUPI9NYndUnr3LnatZu9O',NULL,'2018-09-06 13:28:12','2018-09-06 13:28:37',1,1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
