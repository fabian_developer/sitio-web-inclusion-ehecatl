-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-09-2018 a las 18:02:05
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `voluntariado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblactividad`
--

CREATE TABLE `tblactividad` (
  `intidactividad` int(10) NOT NULL,
  `vchnombre` varchar(250) NOT NULL,
  `vchdescripcion` varchar(500) NOT NULL,
  `vchfecha` varchar(50) NOT NULL,
  `vchhora_inicio` varchar(50) NOT NULL,
  `vchhora_termino` varchar(50) NOT NULL,
  `intnum_voluntarios` int(5) NOT NULL,
  `intnum_ninios` int(5) NOT NULL,
  `intidlugar` int(10) NOT NULL,
  `intid_tipo_transporte` int(10) NOT NULL,
  `vchestado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblactividad`
--

INSERT INTO `tblactividad` (`intidactividad`, `vchnombre`, `vchdescripcion`, `vchfecha`, `vchhora_inicio`, `vchhora_termino`, `intnum_voluntarios`, `intnum_ninios`, `intidlugar`, `intid_tipo_transporte`, `vchestado`) VALUES
(1, 'Paseo al parque ecológico', 'Paseo al parque ecológico', '2018-09-12', '02:00', '01:03', 1, 1, 1, 1, 'En espera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblactividad_voluntariado`
--

CREATE TABLE `tblactividad_voluntariado` (
  `intid_activ_volun` int(10) NOT NULL,
  `intidvoluntariado` int(10) NOT NULL,
  `intidactividad` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbldiscapacidad`
--

CREATE TABLE `tbldiscapacidad` (
  `intIdDiscapacidad` int(11) NOT NULL,
  `vchNombre` varchar(30) NOT NULL,
  `vchDescripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbldisponibilidad`
--

CREATE TABLE `tbldisponibilidad` (
  `intiddisponibilidad` int(10) NOT NULL,
  `vchdisponibilidad` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tblnoticias` (
  `intidnoticia` int(10) NOT NULL AUTO_INCREMENT,
  `vchtitulo` varchar(100) NOT NULL,
  `vchdescripcion` varchar(1000) NOT NULL,
  `vchfecha` varchar(50) NOT NULL,
  `vchimagen` varchar(100) NOT NULL,
  PRIMARY KEY(intidnoticia)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




--
-- Volcado de datos para la tabla `tbldisponibilidad`
--

INSERT INTO `tbldisponibilidad` (`intiddisponibilidad`, `vchdisponibilidad`) VALUES
(1, 'Mañana'),
(2, 'Tarde'),
(3, 'Fines de semana'),
(4, 'Indiferente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblhistorial`
--

CREATE TABLE `tblhistorial` (
  `inIdNinio` int(11) DEFAULT NULL,
  `vchAntHereditario` varchar(300) DEFAULT NULL,
  `vchEnferAnterior` varchar(300) DEFAULT NULL,
  `vchOperaciones` varchar(300) DEFAULT NULL,
  `vchAlergias` varchar(300) DEFAULT NULL,
  `vchEnferActual` varchar(300) DEFAULT NULL,
  `vchObservaciones` varchar(300) DEFAULT NULL,
  `vchComprobacion` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbllugar`
--

CREATE TABLE `tbllugar` (
  `intidlugar` int(10) NOT NULL,
  `vchlugar` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbllugar`
--

INSERT INTO `tbllugar` (`intidlugar`, `vchlugar`) VALUES
(1, 'Parque ecologico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblmultimedia`
--

CREATE TABLE `tblmultimedia` (
  `intid_multimedia` int(10) NOT NULL,
  `vchtitulo` varchar(250) NOT NULL,
  `vchdescripcion` varchar(1000) NOT NULL,
  `vcharchivo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblninio`
--

CREATE TABLE `tblninio` (
  `inIdNinio` int(11) NOT NULL,
  `vchNombre` varchar(40) NOT NULL,
  `vchApellidoPa` varchar(40) NOT NULL,
  `vchApellidoMa` varchar(40) NOT NULL,
  `vchSexo` varchar(15) NOT NULL,
  `dteFechaNac` date NOT NULL,
  `vchRefDomicilio` varchar(240) NOT NULL,
  `intEdad` int(11) NOT NULL,
  `dbePeso` double DEFAULT NULL,
  `dbeEstatura` double DEFAULT NULL,
  `vchTipoSangre` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblninio`
--

INSERT INTO `tblninio` (`inIdNinio`, `vchNombre`, `vchApellidoPa`, `vchApellidoMa`, `vchSexo`, `dteFechaNac`, `vchRefDomicilio`, `intEdad`, `dbePeso`, `dbeEstatura`, `vchTipoSangre`) VALUES
(3, 'Alberto', 'Lara', 'Hernandez', 'Masculino', '2018-03-07', 'Col. colalambre', 15, 60, 1.64, 'O Negativo'),
(5, 'Luis', 'Hernandez', 'Garcia', 'Masculino', '2018-03-12', 'Colalambre', 18, 12, 1.68, 'Positivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblniniodiscap`
--

CREATE TABLE `tblniniodiscap` (
  `intIdNinioDisc` int(11) NOT NULL,
  `inIdNinio` int(11) DEFAULT NULL,
  `intIdDiscapacidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblniniopadre`
--

CREATE TABLE `tblniniopadre` (
  `intIdNinioPadre` int(11) NOT NULL,
  `intIdPadre` int(11) DEFAULT NULL,
  `inIdNinio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblocupacion`
--

CREATE TABLE `tblocupacion` (
  `intidocupacion` int(10) NOT NULL,
  `vchocupacion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblocupacion`
--

INSERT INTO `tblocupacion` (`intidocupacion`, `vchocupacion`) VALUES
(1, 'Estudiante'),
(2, 'Profesor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpadre`
--

CREATE TABLE `tblpadre` (
  `intIdPadre` int(11) NOT NULL,
  `vchNombre` varchar(40) NOT NULL,
  `vchApellidoPa` varchar(40) NOT NULL,
  `vchApellidoMa` varchar(40) NOT NULL,
  `vchSexo` varchar(12) NOT NULL,
  `dteFechaNac` date NOT NULL,
  `vchDomicilio` varchar(240) NOT NULL,
  `vchReferenciaDom` varchar(240) NOT NULL,
  `vchEscolaridad` varchar(80) NOT NULL,
  `vchOcupacion` varchar(50) NOT NULL,
  `vchTelMovil` varchar(15) NOT NULL,
  `vchTelFijo` varchar(15) NOT NULL,
  `intIdTipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblpadre`
--

INSERT INTO `tblpadre` (`intIdPadre`, `vchNombre`, `vchApellidoPa`, `vchApellidoMa`, `vchSexo`, `dteFechaNac`, `vchDomicilio`, `vchReferenciaDom`, `vchEscolaridad`, `vchOcupacion`, `vchTelMovil`, `vchTelFijo`, `intIdTipo`) VALUES
(1, 'Juan ', 'vite', 'Hernandez', 'maasculino', '1975-07-16', 'zaragoza', 'porton verde', 'Secundaria', 'abogafo', '771154504', '7711763412', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbltipofamiliar`
--

CREATE TABLE `tbltipofamiliar` (
  `intIdTipoFam` int(11) NOT NULL,
  `vchTipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbltipofamiliar`
--

INSERT INTO `tbltipofamiliar` (`intIdTipoFam`, `vchTipo`) VALUES
(1, 'Padre'),
(2, 'Madre'),
(3, 'Tutor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbltipo_transporte`
--

CREATE TABLE `tbltipo_transporte` (
  `intid_tipo_transporte` int(10) NOT NULL,
  `vchtipo_transporte` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbltipo_transporte`
--

INSERT INTO `tbltipo_transporte` (`intid_tipo_transporte`, `vchtipo_transporte`) VALUES
(1, 'Caminando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblvoluntariado`
--

CREATE TABLE `tblvoluntariado` (
  `intidvoluntariado` int(10) NOT NULL,
  `vchnombre` varchar(250) NOT NULL,
  `vchapellidos` varchar(250) NOT NULL,
  `vchdireccion` varchar(500) NOT NULL,
  `vchtelefono` varchar(15) NOT NULL,
  `vchemail` varchar(250) NOT NULL,
  `vchgenero` varchar(50) NOT NULL,
  `vchocupacion` varchar(250) NOT NULL,
  `vchfecha_nacimiento` varchar(50) NOT NULL,
  `vchdisponibilidad` varchar(50) NOT NULL,
  `vchporque` varchar(500) NOT NULL,
  `vchfoto_perfil` varchar(250) DEFAULT NULL,
  `vchfecha_registro` varchar(100) NOT NULL,
  `vchtestimonio` varchar(1000) DEFAULT NULL,
  `vchestado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblvoluntariado`
--

INSERT INTO `tblvoluntariado` (`intidvoluntariado`, `vchnombre`, `vchapellidos`, `vchdireccion`, `vchtelefono`, `vchemail`, `vchgenero`, `vchocupacion`, `vchfecha_nacimiento`, `vchdisponibilidad`, `vchporque`, `vchfoto_perfil`, `vchfecha_registro`, `vchtestimonio`, `vchestado`) VALUES
(27, 'Fabian', 'Santiago Hernández', 'Loc. Acuatempa Huejutla de Reyes Hidalgo calle 15', '7713023710', 'tecnologias1997@gmail.com', 'Masculino', 'Estudiante', '2000-09-05', 'Fines de semana', 'porque tengo mucho tiempo libre y me isdfhdkhkjdg nfgrgr', '1536257469fabian.jpg', 'Jueves, 6 de Septiembre de 2018 13:9:26', 'no seas mamut', 'Aceptado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo_usuario` int(5) NOT NULL,
  `confirmado` tinyint(1) NOT NULL,
  `codigo_confirmacion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `tipo_usuario`, `confirmado`, `codigo_confirmacion`) VALUES
(53, 'example', 'example@example.com', '$2y$10$kVWJ55sAiCI82g64rqbciei6ON9ahfTWHo6jkdOZstikSz6f9BzGm', 'qTSF8mZy1uqd45RrObCw9uMsU4qNcLfhqtd9ljoT7oWX3UJnamcnc96owtGD', NULL, NULL, 0, 1, NULL),
(54, 'Fabian Santiago Hernández', 'tecnologias1997@gmail.com', '$2y$10$GV6GwcxAW5X7r.FeS0sJZOPRukdzHQa.bUPI9NYndUnr3LnatZu9O', NULL, '2018-09-06 18:28:12', '2018-09-06 18:28:37', 1, 1, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblactividad`
--
ALTER TABLE `tblactividad`
  ADD PRIMARY KEY (`intidactividad`),
  ADD KEY `Relacion_tbltransporte` (`intid_tipo_transporte`),
  ADD KEY `Relacion_tbllugar` (`intidlugar`);

--
-- Indices de la tabla `tblactividad_voluntariado`
--
ALTER TABLE `tblactividad_voluntariado`
  ADD PRIMARY KEY (`intid_activ_volun`),
  ADD KEY `Relacion_tblactividad_` (`intidactividad`),
  ADD KEY `Relacion_tblvoluntariado` (`intidvoluntariado`);

--
-- Indices de la tabla `tbldiscapacidad`
--
ALTER TABLE `tbldiscapacidad`
  ADD PRIMARY KEY (`intIdDiscapacidad`);

--
-- Indices de la tabla `tbldisponibilidad`
--
ALTER TABLE `tbldisponibilidad`
  ADD PRIMARY KEY (`intiddisponibilidad`);

--
-- Indices de la tabla `tblhistorial`
--
ALTER TABLE `tblhistorial`
  ADD KEY `inIdNinio` (`inIdNinio`),
  ADD KEY `inIdNinio_2` (`inIdNinio`);

--
-- Indices de la tabla `tbllugar`
--
ALTER TABLE `tbllugar`
  ADD PRIMARY KEY (`intidlugar`);

--
-- Indices de la tabla `tblmultimedia`
--
ALTER TABLE `tblmultimedia`
  ADD PRIMARY KEY (`intid_multimedia`);

--
-- Indices de la tabla `tblninio`
--
ALTER TABLE `tblninio`
  ADD PRIMARY KEY (`inIdNinio`);

--
-- Indices de la tabla `tblniniodiscap`
--
ALTER TABLE `tblniniodiscap`
  ADD PRIMARY KEY (`intIdNinioDisc`),
  ADD KEY `relacionNinio` (`inIdNinio`),
  ADD KEY `relacionDiscapacidad` (`intIdDiscapacidad`);

--
-- Indices de la tabla `tblniniopadre`
--
ALTER TABLE `tblniniopadre`
  ADD PRIMARY KEY (`intIdNinioPadre`),
  ADD KEY `relacion_Padre` (`intIdPadre`),
  ADD KEY `relacion_ninio` (`inIdNinio`);

--
-- Indices de la tabla `tblocupacion`
--
ALTER TABLE `tblocupacion`
  ADD PRIMARY KEY (`intidocupacion`);

--
-- Indices de la tabla `tblpadre`
--
ALTER TABLE `tblpadre`
  ADD PRIMARY KEY (`intIdPadre`),
  ADD KEY `relacion_TipoFamiliar` (`intIdTipo`);

--
-- Indices de la tabla `tbltipofamiliar`
--
ALTER TABLE `tbltipofamiliar`
  ADD PRIMARY KEY (`intIdTipoFam`);

--
-- Indices de la tabla `tbltipo_transporte`
--
ALTER TABLE `tbltipo_transporte`
  ADD PRIMARY KEY (`intid_tipo_transporte`);

--
-- Indices de la tabla `tblvoluntariado`
--
ALTER TABLE `tblvoluntariado`
  ADD PRIMARY KEY (`intidvoluntariado`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblactividad`
--
ALTER TABLE `tblactividad`
  MODIFY `intidactividad` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tblactividad_voluntariado`
--
ALTER TABLE `tblactividad_voluntariado`
  MODIFY `intid_activ_volun` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbldiscapacidad`
--
ALTER TABLE `tbldiscapacidad`
  MODIFY `intIdDiscapacidad` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbldisponibilidad`
--
ALTER TABLE `tbldisponibilidad`
  MODIFY `intiddisponibilidad` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbllugar`
--
ALTER TABLE `tbllugar`
  MODIFY `intidlugar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tblmultimedia`
--
ALTER TABLE `tblmultimedia`
  MODIFY `intid_multimedia` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblninio`
--
ALTER TABLE `tblninio`
  MODIFY `inIdNinio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tblniniodiscap`
--
ALTER TABLE `tblniniodiscap`
  MODIFY `intIdNinioDisc` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblniniopadre`
--
ALTER TABLE `tblniniopadre`
  MODIFY `intIdNinioPadre` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblocupacion`
--
ALTER TABLE `tblocupacion`
  MODIFY `intidocupacion` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tblpadre`
--
ALTER TABLE `tblpadre`
  MODIFY `intIdPadre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbltipofamiliar`
--
ALTER TABLE `tbltipofamiliar`
  MODIFY `intIdTipoFam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbltipo_transporte`
--
ALTER TABLE `tbltipo_transporte`
  MODIFY `intid_tipo_transporte` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tblvoluntariado`
--
ALTER TABLE `tblvoluntariado`
  MODIFY `intidvoluntariado` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblactividad`
--
ALTER TABLE `tblactividad`
  ADD CONSTRAINT `Relacion_tbllugar` FOREIGN KEY (`intidlugar`) REFERENCES `tbllugar` (`intidlugar`),
  ADD CONSTRAINT `Relacion_tbltransporte` FOREIGN KEY (`intid_tipo_transporte`) REFERENCES `tbltipo_transporte` (`intid_tipo_transporte`);

--
-- Filtros para la tabla `tblactividad_voluntariado`
--
ALTER TABLE `tblactividad_voluntariado`
  ADD CONSTRAINT `Relacion_tblactividad_` FOREIGN KEY (`intidactividad`) REFERENCES `tblactividad` (`intidactividad`),
  ADD CONSTRAINT `Relacion_tblvoluntariado` FOREIGN KEY (`intidvoluntariado`) REFERENCES `tblvoluntariado` (`intidvoluntariado`);

--
-- Filtros para la tabla `tblhistorial`
--
ALTER TABLE `tblhistorial`
  ADD CONSTRAINT `tblhistorial_ibfk_1` FOREIGN KEY (`inIdNinio`) REFERENCES `tblninio` (`inIdNinio`);

--
-- Filtros para la tabla `tblniniodiscap`
--
ALTER TABLE `tblniniodiscap`
  ADD CONSTRAINT `relacionDiscapacidad` FOREIGN KEY (`intIdDiscapacidad`) REFERENCES `tbldiscapacidad` (`intIdDiscapacidad`),
  ADD CONSTRAINT `relacionNinio` FOREIGN KEY (`inIdNinio`) REFERENCES `tblninio` (`inIdNinio`);

--
-- Filtros para la tabla `tblniniopadre`
--
ALTER TABLE `tblniniopadre`
  ADD CONSTRAINT `relacion_Padre` FOREIGN KEY (`intIdPadre`) REFERENCES `tblpadre` (`intIdPadre`),
  ADD CONSTRAINT `relacion_ninio` FOREIGN KEY (`inIdNinio`) REFERENCES `tblninio` (`inIdNinio`);

--
-- Filtros para la tabla `tblpadre`
--
ALTER TABLE `tblpadre`
  ADD CONSTRAINT `relacion_TipoFamiliar` FOREIGN KEY (`intIdTipo`) REFERENCES `tbltipofamiliar` (`intIdTipoFam`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
