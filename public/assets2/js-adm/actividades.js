window.onload = function() {
    function tiempo() {
        $("#contenedor").slideUp();
    };
    window.setTimeout(tiempo, 1000);
}
$(document).on('ready', function() {
    tabla_actividades();
    // $("#btnNuevoRegistroActividad").on('click', function (event) {
    //     $('#frm-modal-actividades').modal('show');
    //     reset();
    //     $('#titulo_modal').html('Nuevo registro');
    // });
    // $("#btnCerrar").on('click', function (event) {
    //     $('#frm-modal-actividades').modal('hide');
    // });
    // GuardarRegistro();
});
var tabla_actividades = function() {
    var tabla = $('#tabla_actividades').DataTable({
        "processing": true,
        // "serverSide": true,
        "destroy": true,
        // "select": true,
        "autoWidth": true,
        "ajax": "actividades.tabla_actividades",
        "columns": [{
            "data": "clave_actividad",
            "visible": false
        }, {
            "data": "actividad"
        }, {
            "data": "descripcion"
            //,"visible": false
        }, {
            "data": "fecha",
            "render": function(data) {
                var elem = data.split('-');
                var dia = elem[2];
                var mes = elem[1];
                var ano = elem[0];
                var fecha = dia + "/" + mes + "/" + ano;
                return fecha;
            }
        }, {
            "data": "hora_inicio",
            "visible": false
        }, {
            "data": "hora_termino",
            "visible": false
        }, {
            "data": "num_voluntarios"
            //,
            //"visible": false
        }, {
            "data": "num_ninios"
        }, {
            "data": "clave_lugar",
            "visible": false
        }, {
            "data": "lugar",
            "visible": false
        }, {
            "data": "clave_tipo_transporte",
            "visible": false
        }, {
            "data": "tipo_transporte",
            "visible": false
        }, {
            "data": "estado",
            "render": function(data) {
                if (data == "En espera") {
                    return '<span class="badge badge-pill badge-warning"> ' + data + ' </span>';
                } else if (data == "Aprovado") {
                    return '<span class="badge badge-pill badge-success"> ' + data + ' </span>';
                } else if (data == "Rechazado") {
                    return '<span class="badge badge-pill badge-danger"> ' + data + ' </span>';
                } else if (data == "Completado") {
                    return '<span class="badge badge-pill badge-dark"> ' + data + ' </span>';
                }
                //badge-primary  -> badge-secondary -> badge-success -> badge-danger -> badge-warning -> badge-info -> badge-light -> badge-dark
            }
        }, {
            "defaultContent": "<button type='button' data-toggle='modal' data-target='#frm-modal-actividades' class='ver btn btn-info btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Ver'><i class='mdi mdi-launch'></i> </button>"
        }],
        "language": idioma
    });
    //<button type='button' data-toggle='modal' data-target='#frm-modal-actividades' class='editar btn btn-warning btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Editar'><i class='mdi mdi-pencil'></i> </button><button type='button' data-toggle='modal' data-target='#' class='eliminar btn btn-danger btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Eliminar'><i class='mdi mdi-delete'></i> </button>
    $('#tabla_actividades tbody').on("click", "button.ver", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = $("#clave").val(data.clave_actividad);
        var actividad = $("#actividad").val(data.actividad);
        document.getElementById("actividad").readOnly = true;
        var descripcion = $("#descripcion").val(data.descripcion);
        document.getElementById("descripcion").readOnly = true;
        var fecha = (data.fecha);
        var elem = fecha.split('-');
        var dia = elem[2];
        var mes = elem[1];
        var ano = elem[0];
        var variable = ano + "-" + mes + "-" + dia;
        document.getElementById('fecha').value = variable;
        document.getElementById("fecha").readOnly = true;
        var hora_inicio = $("#hora_inicio").val(data.hora_inicio);
        document.getElementById("hora_inicio").readOnly = true;
        var hora_termino = $("#hora_termino").val(data.hora_termino);
        document.getElementById("hora_termino").readOnly = true;
        var voluntarios = $("#voluntarios").val(data.num_voluntarios);
        document.getElementById("voluntarios").readOnly = true;
        var ninios = $("#ninios").val(data.num_ninios);
        document.getElementById("ninios").readOnly = true;
        var lugar = $("#lugar").val(data.lugar);
        document.getElementById("lugar").readOnly = true;
        var transporte = $("#transporte").val(data.tipo_transporte);
        document.getElementById("transporte").readOnly = true;
        $('#titulo_modal').html('Información detallada');
        // document.getElementById("btnGuardar").disabled = true;
    });
    // $('#tabla_actividades tbody').on("click", "button.editar", function () {
    //     var data = tabla.row($(this).parents("tr")).data();
    //     readOnly_false();
    //     request_input();
    //     var clave = $("#clave").val(data.intidactividad);
    //     var actividad = $("#actividad").val(data.vchnombre);
    //     var descripcion = $("#descripcion").val(data.vchdescripcion);
    //     var fecha = (data.vchfecha);
    //     var elem = fecha.split('-');
    //     var dia = elem[2];
    //     var mes = elem[1];
    //     var ano = elem[0];
    //     var variable = ano + "-" + mes + "-" + dia;
    //     document.getElementById('fecha').value = variable;
    //     var hora_inicio = $("#hora_inicio").val(data.vchhora_inicio);
    //     var hora_termino = $("#hora_termino").val(data.vchhora_termino);
    //     var voluntarios = $("#voluntarios").val(data.intnum_voluntarios);
    //     var ninios = $("#ninios").val(data.intnum_ninios);
    //     var lugar = $("#lugar").val(data.intidlugar);
    //     var transporte = $("#transporte").val(data.intid_tipo_transporte);
    //     $('#titulo_modal').html('Modificar registro');
    // });
    // $('#tabla_actividades tbody').on("click", "button.eliminar", function () {
    //     var data = tabla.row($(this).parents("tr")).data();
    //     var clave = (data.intidactividad);
    //     SwalDelete(clave);
    // });
}
var idioma = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}