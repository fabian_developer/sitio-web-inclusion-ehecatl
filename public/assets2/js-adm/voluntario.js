window.onload = function() {
    function tiempo() {
        $("#contenedor").slideUp();
    };
    window.setTimeout(tiempo, 1000);
}
$(document).on('ready', function() {
    tabla_en_espera();
    CambiarEstatus();
    $("#btnCerrar").on('click', function(event) {
        $('#frm-modal-ver-informacion').modal('hide');
    });
});
var tabla_en_espera = function() {
    var tabla = $('#tabla_en_espera').DataTable({
        "processing": true,
        // "serverSide": true,
        "destroy": true,
        // "select": true,
        "ajax": "voluntario.getdatavoluntario",
        "columns": [{
            "data": "intidvoluntariado",
            "visible": false
        }, {
            "data": "vchnombre"
        }, {
            "data": "vchapellidos"
        }, {
            "data": "vchdireccion",
            "visible": false
        }, {
            "data": "vchtelefono"
        }, {
            "data": "vchemail"
        }, {
            "data": "vchgenero",
            "visible": false
        }, {
            "data": "vchocupacion",
            "visible": false
        }, {
            "data": "vchfecha_nacimiento",
            "visible": false,
            "render": function(data) {
                // valores de la base de datos
                var elem = data.split('-');
                var dia = elem[2];
                var mes = elem[1];
                var ano = elem[0];
                //valores actuales
                var fecha_hoy = new Date();
                var ahora_ano = fecha_hoy.getYear();
                var ahora_mes = fecha_hoy.getMonth() + 1;
                var ahora_dia = fecha_hoy.getDate();
                // realizamos el calculo
                var edad = (ahora_ano + 1900) - ano;
                if (ahora_mes < (mes - 1)) {
                    edad--;
                }
                if (((mes - 1) == ahora_mes) && (ahora_dia < dia)) {
                    edad--;
                }
                if (edad > 1900) {
                    edad -= 1900;
                }
                return edad;
            }
        }, {
            "data": "vchdisponibilidad",
            "visible": false
        }, {
            "data": "vchporque",
            "visible": false
        }, {
            "data": "vchfoto_perfil",
            // "data": "vchFoto",
            "render": function(data) {
                return '<img height="140" width="140" src="../../voluntario/foto/' + data + '"/>';
            }
            //height="150" width="100"
        }, {
            "data": "vchfecha_registro",
            "visible": false
        }, {
            "data": "vchestado",
            "render": function(data) {
                if (data == "En espera") {
                    return '<span class="badge badge-pill badge-warning"> ' + data + ' </span>';
                } else if (data == "Aceptado") {
                    return '<span class="badge badge-pill badge-success"> ' + data + ' </span>';
                } else if (data == "Rechazado") {
                    return '<span class="badge badge-pill badge-danger"> ' + data + ' </span>';
                }
            }
        }, {
            "defaultContent": "<button type='button' data-toggle='modal' data-target='#frm-modal-ver-informacion' class='ver btn btn-info btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Ver'><i class='mdi mdi-account-card-details'></i></button>"
            //<button type='button' class='eliminar btn btn-danger'><i class='mdi mdi-account-box-outline'></i> Aceptar</button>
        }],
        "language": idioma
    });
    //
    $('#tabla_en_espera tbody').on("click", "button.ver", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = $("#clave").val(data.intidvoluntariado);
        var nombre = $("#nombre").val(data.vchnombre);
        var apellidos = $("#apellidos").val(data.vchapellidos);
        var direccion = $("#direccion").val(data.vchdireccion);
        var telefono = $("#telefono").val(data.vchtelefono);
        var email = $("#email").val(data.vchemail);
        var genero = $("#genero").val(data.vchgenero);
        var ocupacion = $("#ocupacion").val(data.vchocupacion);
        //var nacimiento = $("#fecha_nacimiento").val(data.vchfecha_nacimiento);
        var disponibilidad = $("#disponibilidad").val(data.vchdisponibilidad);
        var justificacion = $("#justificacion").val(data.vchporque);
        var foto = (data.vchfoto_perfil);
        document.getElementById('foto').src = "../../voluntario/foto/" + foto;
        //calcula edad
        var dato = (data.vchfecha_nacimiento);
        // valores de la base de datos
        var elem = dato.split('-');
        var dia = elem[2];
        var mes = elem[1];
        var ano = elem[0];
        //valores actuales
        var fecha_hoy = new Date();
        var ahora_ano = fecha_hoy.getYear();
        var ahora_mes = fecha_hoy.getMonth() + 1;
        var ahora_dia = fecha_hoy.getDate();
        // realizamos el calculo
        var edad = (ahora_ano + 1900) - ano;
        if (ahora_mes < (mes - 1)) {
            edad--;
        }
        if (((mes - 1) == ahora_mes) && (ahora_dia < dia)) {
            edad--;
        }
        if (edad > 1900) {
            edad -= 1900;
        }
        var edad_ = $("#fecha_nacimiento").val(edad + " años");
        var estatus = $("#estatus").val(data.vchestado);
    });
}
var CambiarEstatus = function() {
    $("#btnGuardarCambios").on('click', function(event) {
        event.preventDefault();
        var clave = $("#clave").val();
        var estatus = $("#estatus").val();
        var email = $("#email").val();
        var name = $("#nombre").val();
        var name2 = $("#apellidos").val();
        var nombre = name + " " + name2;
        var token = $("#token").val();
        $.ajax({
            method: "post",
            //type: "POST",
            dataType: 'json',
            url: "voluntario.modificaestatus",
            data: {
                clave,
                estatus,
                email,
                nombre,
                _token: token
            }
        }).done(function(data) {
            if (data.error == "calificado") {
                swal({
                    title: "¡Ho, ho!",
                    text: "El voluntario ya fue calificado.",
                    icon: "warning",
                    button: "Aceptar",
                });
            } else {
                swal({
                    title: "¡Bien hecho!",
                    text: "¡El voluntario fue " + estatus + "!",
                    icon: "success",
                    button: "Aceptar",
                });
                var tabla = $('#tabla_en_espera').DataTable();
                tabla.ajax.reload();
                $('#frm-modal-ver-informacion').modal('hide');
            }
        }).fail(function() {
            swal({
                title: "¡Ho, ho!",
                text: "Vaya, algo ha salido mal. Inténtelo de nuevo más tarde.",
                icon: "warning",
                button: "Aceptar",
            });
        }).always(function() {});
    });
}
var idioma = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}