window.onload = function() {
    function tiempo() {
        $("#contenedor").slideUp();
    };
    window.setTimeout(tiempo, 1000);
}
$(document).on('ready', function() {
    tabla_lugares();
    $("#btnCerrar").on('click', function(event) {
        $('#frm-modal-lugares').modal('hide');
    });
    $("#btnNuevoRegistroLugar").on('click', function(event) {
        $('#frm-modal-lugares').modal('show');
        reset();
        $('#titulo_modal').html('Nuevo registro');
    });
    GuardarRegistro();
});

function reset() {
    document.getElementById("lugar").value = "";
    document.getElementById("clave").value = "";
}

function formulario() {
    $('#request_lugar').hide();
    var lugar = document.getElementById('lugar').value;
    if (lugar == null || lugar.length == 0 || /^\s+$/.test(lugar)) {
        $('#request_lugar').html('<div class = "alert alert-danger"> El campo lugar no debe ir vacío o lleno de solamente espacios en blanco </div>');
        $('#request_lugar').show();
        $('#request_lugar').hide(10000);
        $('.request_lugar').hide("fast");
        return false;
    }
    return true;
}
var tabla_lugares = function() {
    var tabla = $('#tabla_lugares').DataTable({
        "processing": true,
        // "serverSide": true,
        "destroy": true,
        // "select": true,
        "ajax": "lugares.getdatalugares",
        "columns": [{
            "data": "intidlugar",
            "visible": false
        }, {
            "data": "vchlugar"
        }, {
            "defaultContent": "<button type='button' data-toggle='modal' data-target='#frm-modal-lugares' class='editar btn btn-warning btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Editar'><i class='mdi mdi-pencil'></i></button><button type='button' data-toggle='modal' data-target='#' class='eliminar btn btn-danger btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Eliminar'><i class='mdi mdi-delete'></i></button>"
        }],
        "language": idioma
    });
    //
    $('#tabla_lugares tbody').on("click", "button.editar", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = $("#clave").val(data.intidlugar);
        var lugar = $("#lugar").val(data.vchlugar);
        $('#titulo_modal').html('Modificar registro');
    });
    $('#tabla_lugares tbody').on("click", "button.eliminar", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = (data.intidlugar);
        SwalDelete(clave);
    });
}
var GuardarRegistro = function() {
    $("#btnGuardar").on('click', function(event) {
        event.preventDefault();
        if (formulario() == true) {
            var clave = $("#clave").val();
            var lugar = $("#lugar").val();
            var token = $("#token").val();
            $.ajax({
                method: "post",
                //type: "POST",
                dataType: 'json',
                url: "lugares.guardardatos",
                data: {
                    clave,
                    lugar,
                    _token: token
                }
            }).done(function(data) {
                if (data.error.length > 0) {
                    var error_html = '';
                    var acomula;
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<li type=square> ' + data.error[count] + '<br>';
                    }
                    acomula = '<div class="alert alert-danger">' + error_html + '</div>';
                    $('#form_output').html(acomula);
                } else {
                    if (data.success == "save") {
                        swal({
                            title: "¡Bien hecho!",
                            text: "¡El registro fue guardado!",
                            icon: "success",
                            button: "Aceptar",
                        });
                        var tabla = $('#tabla_lugares').DataTable();
                        tabla.ajax.reload();
                        $('#frm-modal-lugares').modal('hide');
                        reset();
                    } else if (data.success == "update") {
                        swal({
                            title: "¡Bien hecho!",
                            text: "¡El registro fue actualizado!",
                            icon: "success",
                            button: "Aceptar",
                        });
                        var tabla = $('#tabla_lugares').DataTable();
                        tabla.ajax.reload();
                        $('#frm-modal-lugares').modal('hide');
                        reset();
                    }
                }
            }).fail(function() {
                swal({
                    title: "¡Ho, ho!",
                    text: "Vaya, algo ha salido mal. Inténtelo de nuevo más tarde.",
                    icon: "warning",
                    button: "Aceptar",
                });
            }).always(function() {});
        }
    });
}

function SwalDelete(clave) {
    var token = $("#token").val();
    swal({
        title: "¿Estás seguro?",
        text: "Una vez que se elimine, ¡no podrá recuperar este registro!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: "post",
                dataType: 'json',
                data: {
                    clave,
                    _token: token
                },
                url: "lugares.deletedata"
            }).done(function() {
                swal({
                    title: "¡Bien hecho!",
                    text: "¡Registro eliminado con exito!",
                    icon: "success",
                    button: "Aceptar",
                });
                var tabla = $('#tabla_lugares').DataTable();
                tabla.ajax.reload();
                clave = null;
            }).fail(function() {
                alert("error");
            }).always(function() {
                //alert( "complete" );
            });
        } else {
            swal("El registro es seguro, ¡Eliminación cancelada!");
        }
    });
}
var idioma = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}