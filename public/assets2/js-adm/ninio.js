window.onload = function() {
    function tiempo() {
        $("#contenedor").slideUp();
    };
    window.setTimeout(tiempo, 1000);
}
// 
$(document).on('ready', function() {
    tabla_ninios();
    $("#btnCerrar").on('click', function(event) {
        $('#frm-modal-ver-informacion').modal('hide');
    });
});
var tabla_ninios = function() {
    var tabla = $('#tabla_ninios').DataTable({
        "processing": true,
        "destroy": true,
        "ajax": "ninios.tabla_ninios",
        "columns": [{
            "data": "clave_ninio",
            "visible": false
        }, {
            "data": "nombre"
        }, {
            "data": "apellido_pa"
        }, {
            "data": "apellido_ma"
        }, {
            "data": "genero"
        }, {
            "data": "fecha_nacimiento",
            "visible": false
        }, {
            "data": "domicilio",
            "visible": false
        }, {
            "data": "edad"
        }, {
            "data": "peso",
            "visible": false
        }, {
            "data": "estatura",
            "visible": false
        }, {
            "data": "tipo_sangre",
            "visible": false
        }, {
            "data": "discapacidad"
        }, {
            "data": "descripcion",
            "visible": false
        }, {
            "defaultContent": "<button type='button' data-toggle='modal' data-target='#frm-modal-ver-informacion' class='ver btn btn-info btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Ver todos los datos'><i class='mdi mdi-launch'></i></button>"
        }],
        "language": idioma
    });
    //
    $('#tabla_ninios tbody').on("click", "button.ver", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = $("#clave").val(data.clave_ninio);
        var nombre = $("#nombre").val(data.nombre);
        apellido_pa = (data.apellido_pa);
        apellido_ma = (data.apellido_ma);
        var apellidos = $("#apellidos").val(apellido_pa + " " + apellido_ma);
        var genero = $("#genero").val(data.genero);
        var nombre = $("#fecha_de_nacimiento").val(data.fecha_nacimiento);
        var direccion = $("#direccion").val(data.domicilio);
        var edad = $("#edad").val(data.edad);
        var peso = $("#peso").val(data.peso);
        var estatura = $("#estatura").val(data.estatura);
        var tipo_de_sangre = $("#tipo_de_sangre").val(data.tipo_sangre);
        var discapacidad = $("#discapacidad").val(data.discapacidad);
        var descripcion = $("#descripcion").val(data.descripcion);
    });
}
var idioma = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}