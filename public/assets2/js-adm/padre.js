window.onload = function() {
    function tiempo() {
        $("#contenedor").slideUp();
    };
    window.setTimeout(tiempo, 1000);
}
$(document).on('ready', function() {
    tabla_ninios();
    $("#btnCerrar").on('click', function(event) {
        $('#frm-modal-ver-informacion').modal('hide');
    });
});
var tabla_ninios = function() {
    var tabla = $('#tabla_padres').DataTable({
        "processing": true,
        "destroy": true,
        "ajax": "padres.tabla_padres",
        "columns": [{
            "data": "clave_padre",
            "visible": false
        }, {
            "data": "padre"
        }, {
            "data": "apellido_pa"
        }, {
            "data": "apellido_ma"
        }, {
            "data": "genero",
            "visible": false
        }, {
            "data": "fecha_nacimiento",
            "visible": false
        }, {
            "data": "domicilio",
            "visible": false
        }, {
            "data": "referencia",
            "visible": false
        }, {
            "data": "escolaridad",
            "visible": false
        }, {
            "data": "ocupacion",
            "visible": false
        }, {
            "data": "telefono"
        }, {
            "data": "telefono_fijo",
            "visible": false
        }, {
            "data": "ninio"
        }, {
            "data": "ninio_ape_pa"
        }, {
            "data": "ninio_ape_ma"
        }, {
            "defaultContent": "<button type='button' data-toggle='modal' data-target='#frm-modal-ver-informacion' class='ver btn btn-info btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Ver todos los datos'><i class='mdi mdi-launch'></i></button>"
        }],
        "language": idioma
    });
    //
    $('#tabla_padres tbody').on("click", "button.ver", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = $("#clave").val(data.clave_ninio);
        var padre = $("#nombre").val(data.padre);
        apellido_pa = (data.apellido_pa);
        apellido_ma = (data.apellido_ma);
        var apellidos = $("#apellidos").val(apellido_pa + " " + apellido_ma);
        var genero = $("#genero").val(data.genero);
        var fecha_nacimiento = $("#fecha_de_nacimiento").val(data.fecha_nacimiento);
        var domicilio = $("#domicilio").val(data.domicilio);
        var referencia = $("#referencia").val(data.referencia);
        var escolaridad = $("#escolaridad").val(data.escolaridad);
        var ocupacion = $("#ocupacion").val(data.ocupacion);
        var telefono = $("#telefono").val(data.telefono);
        var telefono_fijo = $("#telefono_fijo").val(data.telefono_fijo);
        var hijo = $("#hijo").val(data.ninio);
        apelli_pa = (data.ninio_ape_pa);
        apelli_ma = (data.ninio_ape_ma);
        var apellidos_hijo = $("#apellidos_hijo").val(apelli_pa + " " + apelli_ma);
    });
}
var idioma = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}