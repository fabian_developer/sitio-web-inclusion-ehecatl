$(document).on('ready', function() {
    tabla_actividades();
    $("#btnNuevoRegistroActividad").on('click', function(event) {
        $('#frm-modal-actividades').modal('show');
        reset();
        $('#titulo_modal').html('Nuevo registro');
    });
    $("#btnCerrar").on('click', function(event) {
        $('#frm-modal-actividades').modal('hide');
    });
    GuardarRegistro();
});
var GuardarRegistro = function() {
    $("#btnGuardar").on('click', function(event) {
        event.preventDefault();
        if (formulario() == true) {
            var clave = $("#clave").val();
            var actividad = $("#actividad").val();
            var descripcion = $("#descripcion").val();
            var fecha = $("#fecha").val();
            var hora_inicio = $("#hora_inicio").val();
            var hora_termino = $("#hora_termino").val();
            var voluntarios = $("#voluntarios").val();
            var ninios = $('#ninios').val();
            var lugar = $('#lugar').val();
            var transporte = $('#transporte').val();
            var token = $("#token").val();
            $.ajax({
                method: "post",
                //type: "POST",
                dataType: 'json',
                url: "actividades.guardardatos",
                data: {
                    clave,
                    actividad,
                    descripcion,
                    fecha,
                    hora_inicio,
                    hora_termino,
                    voluntarios,
                    ninios,
                    lugar,
                    transporte,
                    _token: token
                }
            }).done(function(data) {
                if (data.error.length > 0) {
                    var error_html = '';
                    var acomula;
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<li type=square> ' + data.error[count] + '<br>';
                    }
                    acomula = '<div class="alert alert-danger">' + error_html + '</div>';
                    $('#form_output').html(acomula);
                } else {
                    if (data.success == "save") {
                        swal({
                            title: "¡Bien hecho!",
                            text: "¡El registro fue guardado!",
                            icon: "success",
                            button: "Aceptar",
                        });
                        var tabla = $('#tabla_actividades').DataTable();
                        tabla.ajax.reload();
                        $('#frm-modal-actividades').modal('hide');
                        reset();
                    } else if (data.success == "update") {
                        swal({
                            title: "¡Bien hecho!",
                            text: "¡El registro fue actualizado!",
                            icon: "success",
                            button: "Aceptar",
                        });
                        var tabla = $('#tabla_actividades').DataTable();
                        tabla.ajax.reload();
                        $('#frm-modal-actividades').modal('hide');
                        reset();
                    } else if (data.success == "calificado") {
                        swal("Ho, ho!", "La actividad ya fue calificada, ¡Ya no puede editar información!");
                        $('#frm-modal-actividades').modal('hide');
                    } else if (data.success == "fecha_repetida") {
                        swal("Ho, ho!", "¡No puede registrar varias actividades en la misma fecha!");
                    }
                }
            }).fail(function() {
                swal({
                    title: "¡Ho, ho!",
                    text: "Vaya, algo ha salido mal. Inténtelo de nuevo más tarde.",
                    icon: "warning",
                    button: "Aceptar",
                });
            }).always(function() {});
        }
    });
}

function reset() {
    document.getElementById('clave').value = "";
    document.getElementById('actividad').value = "";
    document.getElementById('descripcion').value = "";
    document.getElementById('fecha').value = "";
    document.getElementById('hora_inicio').value = "";
    document.getElementById('hora_termino').value = "";
    document.getElementById('voluntarios').value = "";
    document.getElementById('ninios').value = "";
    readOnly_false();
}

function readOnly_false() {
    document.getElementById("clave").readOnly = false;
    document.getElementById("actividad").readOnly = false;
    document.getElementById("descripcion").readOnly = false;
    document.getElementById("fecha").readOnly = false;
    document.getElementById("hora_inicio").readOnly = false;
    document.getElementById("hora_termino").readOnly = false;
    document.getElementById("voluntarios").readOnly = false;
    document.getElementById("ninios").readOnly = false;
    document.getElementById("lugar").disabled = false;
    document.getElementById("transporte").disabled = false;
    document.getElementById("btnGuardar").disabled = false;
}

function request_input() {
    $('#request_actividad').hide();
    $('#request_descripcion').hide();
    $('#request_fecha').hide();
    $('#request_hora_inicio').hide();
    $('#request_hora_termino').hide();
    $('#request_voluntarios').hide();
    $('#request_ninios').hide();
}

function formulario() {
    request_input();
    var actividad = document.getElementById('actividad').value;
    var descripcion = document.getElementById('descripcion').value;
    var fecha = document.getElementById('fecha').value;
    var hora_inicio = document.getElementById('hora_inicio').value;
    var hora_termino = document.getElementById('hora_termino').value;
    var voluntarios = document.getElementById('voluntarios').value;
    var ninios = document.getElementById('ninios').value;
    if (actividad == null || actividad.length == 0 || /^\s+$/.test(actividad)) {
        $('#request_actividad').html('<div class = "alert alert-danger"> El campo actividad no debe ir vacío o lleno de solamente espacios en blanco </div>');
        $('#request_actividad').show();
        return false;
    }
    if (descripcion == null || descripcion.length == 0 || /^\s+$/.test(descripcion)) {
        $('#request_descripcion').html('<div class = "alert alert-danger"> El campo descripción no debe ir vacío o lleno de solamente espacios en blanco </div>');
        $('#request_descripcion').show();
        return false;
    }
    if (fecha == null || fecha.length == 0 || /^\s+$/.test(fecha)) {
        $('#request_fecha').html('<div class = "alert alert-danger"> El campo fecha no debe ir vacío </div>');
        $('#request_fecha').show();
        return false;
    }
    if (hora_inicio == null || hora_inicio.length == 0 || /^\s+$/.test(hora_inicio)) {
        $('#request_hora_inicio').html('<div class = "alert alert-danger"> El campo hora inicio no debe ir vacío </div>');
        $('#request_hora_inicio').show();
        return false;
    }
    if (hora_termino == null || hora_termino.length == 0 || /^\s+$/.test(hora_termino)) {
        $('#request_hora_termino').html('<div class = "alert alert-danger"> El campo hora termino no debe ir vacío </div>');
        $('#request_hora_termino').show();
        return false;
    }
    if (voluntarios == null || voluntarios.length == 0 || /^\s+$/.test(voluntarios)) {
        $('#request_voluntarios').html('<div class = "alert alert-danger"> El campo núm. de voluntarios no debe ir vacío </div>');
        $('#request_voluntarios').show();
        return false;
    }
    if (ninios == null || ninios.length == 0 || /^\s+$/.test(ninios)) {
        $('#request_ninios').html('<div class = "alert alert-danger"> El campo núm. de niños no debe ir vacío </div>');
        $('#request_ninios').show();
        return false;
    }
    return true;
}
var tabla_actividades = function() {
    var tabla = $('#tabla_actividades').DataTable({
        "processing": true,
        // "serverSide": true,
        "destroy": true,
        // "select": true,
        "autoWidth": true,
        "ajax": "actividades.getdataactividades",
        "columns": [{
            "data": "intidactividad",
            "visible": false
        }, {
            "data": "vchnombre"
        }, {
            "data": "vchdescripcion"
            //,"visible": false
        }, {
            "data": "vchfecha",
            "render": function(data) {
                var elem = data.split('-');
                var dia = elem[2];
                var mes = elem[1];
                var ano = elem[0];
                var fecha = dia + "/" + mes + "/" + ano;
                return fecha;
            }
        }, {
            "data": "vchhora_inicio",
            "visible": false
        }, {
            "data": "vchhora_termino",
            "visible": false
        }, {
            "data": "intnum_voluntarios"
            //,
            //"visible": false
        }, {
            "data": "intnum_ninios"
        }, {
            "data": "intidlugar",
            "visible": false
        }, {
            "data": "intid_tipo_transporte",
            "visible": false
        }, {
            "data": "vchestado",
            "render": function(data) {
                if (data == "En espera") {
                    return '<span class="badge badge-pill badge-warning"> ' + data + ' </span>';
                } else if (data == "Aprovado") {
                    return '<span class="badge badge-pill badge-success"> ' + data + ' </span>';
                } else if (data == "Rechazado") {
                    return '<span class="badge badge-pill badge-danger"> ' + data + ' </span>';
                } else if (data == "Completado") {
                    return '<span class="badge badge-pill badge-dark"> ' + data + ' </span>';
                }
                //badge-primary  -> badge-secondary -> badge-success -> badge-danger -> badge-warning -> badge-info -> badge-light -> badge-dark
            }
        }, {
            "defaultContent": "<button type='button' data-toggle='modal' data-target='#frm-modal-actividades' class='ver btn btn-info btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Ver'><i class='mdi mdi-launch'></i> </button><button type='button' data-toggle='modal' data-target='#frm-modal-actividades' class='editar btn btn-warning btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Editar'><i class='mdi mdi-pencil'></i> </button><button type='button' data-toggle='modal' data-target='#' class='eliminar btn btn-danger btn-outline btn-rounded btn-icon' data-toggle='tooltip' data-placement='top' title='Eliminar'><i class='mdi mdi-delete'></i> </button>"
        }],
        "language": idioma
    });
    //
    $('#tabla_actividades tbody').on("click", "button.ver", function() {
        var data = tabla.row($(this).parents("tr")).data();
        request_input();
        var clave = $("#clave").val(data.intidactividad);
        var actividad = $("#actividad").val(data.vchnombre);
        document.getElementById("actividad").readOnly = true;
        var descripcion = $("#descripcion").val(data.vchdescripcion);
        document.getElementById("descripcion").readOnly = true;
        var fecha = (data.vchfecha);
        var elem = fecha.split('-');
        var dia = elem[2];
        var mes = elem[1];
        var ano = elem[0];
        var variable = ano + "-" + mes + "-" + dia;
        document.getElementById('fecha').value = variable;
        document.getElementById("fecha").readOnly = true;
        var hora_inicio = $("#hora_inicio").val(data.vchhora_inicio);
        document.getElementById("hora_inicio").readOnly = true;
        var hora_termino = $("#hora_termino").val(data.vchhora_termino);
        document.getElementById("hora_termino").readOnly = true;
        var voluntarios = $("#voluntarios").val(data.intnum_voluntarios);
        document.getElementById("voluntarios").readOnly = true;
        var ninios = $("#ninios").val(data.intnum_ninios);
        document.getElementById("ninios").readOnly = true;
        var lugar = $("#lugar").val(data.intidlugar);
        document.getElementById("lugar").disabled = true;
        var transporte = $("#transporte").val(data.intid_tipo_transporte);
        document.getElementById("transporte").disabled = true;
        $('#titulo_modal').html('Información detallada');
        document.getElementById("btnGuardar").disabled = true;
    });
    $('#tabla_actividades tbody').on("click", "button.editar", function() {
        var data = tabla.row($(this).parents("tr")).data();
        readOnly_false();
        request_input();
        var clave = $("#clave").val(data.intidactividad);
        var actividad = $("#actividad").val(data.vchnombre);
        var descripcion = $("#descripcion").val(data.vchdescripcion);
        var fecha = (data.vchfecha);
        var elem = fecha.split('-');
        var dia = elem[2];
        var mes = elem[1];
        var ano = elem[0];
        var variable = ano + "-" + mes + "-" + dia;
        document.getElementById('fecha').value = variable;
        var hora_inicio = $("#hora_inicio").val(data.vchhora_inicio);
        var hora_termino = $("#hora_termino").val(data.vchhora_termino);
        var voluntarios = $("#voluntarios").val(data.intnum_voluntarios);
        var ninios = $("#ninios").val(data.intnum_ninios);
        var lugar = $("#lugar").val(data.intidlugar);
        var transporte = $("#transporte").val(data.intid_tipo_transporte);
        $('#titulo_modal').html('Modificar registro');
    });
    $('#tabla_actividades tbody').on("click", "button.eliminar", function() {
        var data = tabla.row($(this).parents("tr")).data();
        var clave = (data.intidactividad);
        SwalDelete(clave);
    });
}

function SwalDelete(clave) {
    var token = $("#token").val();
    swal({
        title: "¿Estás seguro?",
        text: "Una vez que se elimine, ¡no podrá recuperar este registro!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: "post",
                dataType: 'json',
                data: {
                    clave,
                    _token: token
                },
                url: "actividades.deletedata"
            }).done(function(data) {
                if (data.success == "success") {
                    swal({
                        title: "¡Bien hecho!",
                        text: "¡Registro eliminado con exito!",
                        icon: "success",
                        button: "Aceptar",
                    });
                    var tabla = $('#tabla_actividades').DataTable();
                    tabla.ajax.reload();
                    clave = null;
                } else if (data.success == "calificado") {
                    swal("Ho, ho!", "Solo puede borrar el registro que no haya sido calificado!");
                }
            }).fail(function() {
                alert("error");
            }).always(function() {
                //alert( "complete" );
            });
        } else {
            swal("El registro es seguro, ¡Eliminación cancelada!");
        }
    });
}
var idioma = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}