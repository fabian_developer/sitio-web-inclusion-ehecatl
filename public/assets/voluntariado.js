$(document).on('ready', function () {
    $("#registro").hide();
    $("#btnvoluntariado").on('click', function (event) {
        event.preventDefault();
        $("#informacion").hide();
        $("#registro").show();
        window.scroll(0, 0);
        var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
        var f = new Date();
        var fecha = (diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());
        $("#fecha_registro").val(fecha);
    });
    guardar();
});

function request_input() {
    $('#request_nombre').hide();
    $('#request_apellidos').hide();
    $('#request_direccion').hide();
    $('#request_telefono').hide();
    $('#request_email').hide();
    $('#request_fecha_de_nacimiento').hide();
    $('#request_justificacion').hide();
    $('#request_foto').hide();
}

function formulario() {
    request_input();
    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var direccion = document.getElementById('direccion').value;
    var telefono = document.getElementById('telefono').value;
    var email = document.getElementById('email').value;
    var fecha_de_nacimiento = document.getElementById('fecha_de_nacimiento').value;
    var justificacion = document.getElementById('justificacion').value;
    var extension_foto = document.getElementById("foto_perfil").value.split('.').pop().toLowerCase();
    if (nombre == null || nombre.length == 0 || /^\s+$/.test(nombre)) {
        $('#request_nombre').html('<div class = "alert alert-danger"> El campo nombre no debe ir vacío o lleno de solamente espacios en blanco </div>');
        $('#request_nombre').show();
        return false;
    }
    if (apellidos == null || apellidos.length == 0 || /^\s+$/.test(apellidos)) {
        $('#request_apellidos').html('<div class = "alert alert-danger"> El campo apellidos no debe ir vacío o lleno de solamente espacios en blanco </div>');
        $('#request_apellidos').show();
        return false;
    }
    if (direccion == null || direccion.length == 0 || /^\s+$/.test(direccion)) {
        $('#request_direccion').html('<div class = "alert alert-danger"> El campo dirección no debe ir vacío </div>');
        $('#request_direccion').show();
        return false;
    }
    if (telefono == null || telefono.length == 0 || /^\s+$/.test(telefono)) {
        $('#request_telefono').html('<div class = "alert alert-danger"> El campo telefono no debe ir vacío </div>');
        $('#request_telefono').show();
        return false;
    }
    if (email == null || email.length == 0 || /^\s+$/.test(email)) {
        $('#request_email').html('<div class = "alert alert-danger"> El campo correo electronico no debe ir vacío </div>');
        $('#request_email').show();
        return false;
    }
    if (fecha_de_nacimiento == null || fecha_de_nacimiento.length == 0 || /^\s+$/.test(fecha_de_nacimiento)) {
        $('#request_fecha_de_nacimiento').html('<div class = "alert alert-danger"> El campo fecha de nacimiento no debe ir vacío </div>');
        $('#request_fecha_de_nacimiento').show();
        return false;
    }
    if (justificacion == null || justificacion.length == 0 || /^\s+$/.test(justificacion)) {
        $('#request_justificacion').html('<div class = "alert alert-danger"> El campo justificación no debe ir vacío </div>');
        $('#request_justificacion').show();
        return false;
    }
    return true;
}
$(document).on('change', 'input[type="file"]', function () {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;
    if (fileSize > 10000000) {
        document.getElementById("vchfoto_perfil").value = "";
        alert('El archivo no debe superar los 10 MB');
        this.value = '';
        this.files[0].name = '';
    }
});
var guardar = function () {
    $("#frmvoluntario").on('submit', function (event) {
        event.preventDefault();
        if (formulario() == true) {
            var frmvoluntario = $(this).serialize();
            var extension = document.getElementById("foto_perfil").value.split('.').pop().toLowerCase();
            if (extension != '') {
                if (jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1) {
                    //var fileName = this.files[0].name;
                    alert("Imagen no valido");
                    document.getElementById("foto_perfil").value = "";
                } else {
                    $.ajax({
                        method: "post",
                        dataType: 'json',
                        url: "voluntariado.guardardatos",
                        data: new FormData(this),
                        contentType: false,
                        processData: false,
                    }).done(function (data) {
                        if (data.error.length > 0) {
                            var error_html = '';
                            var acomula;
                            for (var count = 0; count < data.error.length; count++) {
                                error_html += '<li type=square> ' + data.error[count] + '<br>';
                            }
                            acomula = '<div class="alert alert-danger">' + error_html + '</div>';
                            $('#form_output').html(acomula);
                        } else {
                            swal({
                                title: "¡Bien hecho!",
                                text: "En breve recibirá una respuesta por via correo electrónico",
                                icon: "success",
                                button: "Aceptar",
                            });
                            $("#informacion").show();
                            $("#registro").hide();
                            $('#form_output').remove();
                        }
                    }).fail(function () {
                        swal({
                            title: "¡Ho, ho!",
                            text: "Vaya, algo ha salido mal. Inténtelo de nuevo más tarde.",
                            icon: "warning",
                            button: "Aceptar",
                        });
                    }).always(function () {});
                }
            }
        }
    });
}