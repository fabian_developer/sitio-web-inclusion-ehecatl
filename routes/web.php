<?php

Route::get('/bienvenida', function () {
    return view('email/email_bienvenida');
});

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return redirect('inicio');
});

// Route::get('/conocenos', function () {
//     return view('conocenos');
// });

// Route::get('/actualidad', function () {
//     return view('actualidad');
// });

Route::get('/contacto', function () {
    return view('contacto');
});

Route::get('/que_hacemos', function () {
    return view('que_hacemos');
});

// Route::get('/noticia', function () {
//     return view('noticia');
// });

Route::get('conocenos', 'ConocenosController@index');

Route::get('inicio', 'InicioController@index');
Route::get('actualidad', 'NoticiaController@index_actualidad');

// use App\Mail\VoluntarioMail;

// Route::get('/correo', function () {
//     Mail::to('tecnologias1997@gmail.com')->send(new VoluntarioMail);
//     return view('hola');

// });

Auth::routes();

//vista para la pagina voluntariado
Route::get('voluntariado', 'VoluntarioController@index'); //vista voluntario
//guardar datos del registro para el voluntariado
Route::post('voluntariado.guardardatos', 'VoluntarioController@guardardatos')->name('voluntariado.guardardatos');

//codigo de verificacion
Route::get('/registro/verificacion/{code}', 'VoluntarioController@verify');

Route::get('/noticia/{titulo}', 'NoticiaController@noticia');

//rutas para logueado
Route::middleware(['auth'])->group(function () {
    Route::get('/home', function () {
        //usuario administrador
        if (auth()->user()->tipo_usuario == 0) //compara si el rol es administrador
        {
            return redirect('usuario-admin/index'); //redirecciona a la carpeta para administrador
        }
        //usuario normal
        else if (auth()->user()->tipo_usuario == 1) //compara si el rol es usuario normal
        {
            return redirect('usuario-voluntario/index'); //redirecciona a la carpeta para el usuario normal
        }
    });
});

//rutas para administrador
Route::middleware(['auth', 'admin'])->group(function () {

    //***********************************************************************************************
    //vista index
    Route::get('usuario-admin/index', 'InicioController@index_administrador');
    //***********************************************************************************************
    //********************************************************************************************
    //ninios
    Route::get('usuario-admin/ninios', 'NinioController@index_ninio'); //vista
    Route::get('usuario-admin/ninios.tabla_ninios', 'NinioController@tabla_ninios'); //tabla
    //***********************************************************************************************
    //***********************************************************************************************
    //padres
    Route::get('usuario-admin/padres', 'PadreController@index_padre'); //vista
    Route::get('usuario-admin/padres.tabla_padres', 'PadreController@tabla_padres'); //tabla
    //***********************************************************************************************

    //***********************************************************************************************
    //vista voluntario
    Route::get('usuario-admin/voluntarios', 'VoluntarioController@index_voluntario'); //vista
    Route::get('usuario-admin/voluntario.getdatavoluntario', 'VoluntarioController@getdatavoluntario'); //tabla
    Route::post('usuario-admin/voluntario.modificaestatus', 'VoluntarioController@modificaestatus'); //modifica estatus
    //***********************************************************************************************

    //***********************************************************************************************
    //vista lugar
    Route::get('usuario-admin/lugares', 'LugarController@index'); //vista
    Route::get('usuario-admin/lugares.getdatalugares', 'LugarController@getdatalugares'); //tabla
    Route::post('usuario-admin/lugares.guardardatos', 'LugarController@guardardatos'); //guardar
    Route::post('usuario-admin/lugares.deletedata', 'LugarController@deletedata'); //eliminar
    //***********************************************************************************************

    //***********************************************************************************************
    //vista actividad
    Route::get('usuario-admin/actividades', 'ActividadController@index_administrador');
    Route::get('usuario-admin/actividades.tabla_actividades', 'ActividadController@tabla_actividades')->name('actividades.tabla_actividades'); //datatable
    //***********************************************************************************************
});

//rutas para voluntariado
Route::middleware(['auth', 'user'])->group(function () {

    //ver vista index
    Route::get('usuario-voluntario/index', 'VoluntarioController@principal_voluntario')->name('usuario-voluntario.index');

    //vista actividad
    Route::get('usuario-voluntario/actividades', 'ActividadController@index_voluntario'); //vista actividades
    // tabla
    Route::get('usuario-voluntario/actividades.getdataactividades', 'ActividadController@getdataactividades');
    // guardar
    Route::post('usuario-voluntario/actividades.guardardatos', 'ActividadController@guardardatos');
    // eliminar
    Route::post('usuario-voluntario/actividades.deletedata', 'ActividadController@deletedata');

    //perfil
    Route::get('usuario-voluntario/perfil', 'PerfilController@index')->name('usuario-voluntario.perfil');

});
